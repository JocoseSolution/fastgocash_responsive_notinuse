﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace PayWithFC
{
    public class srvFCG
    {
        public string GetPostReqResPayU(string PostString, string Url, string ToHash, string Hashed, string MerchantTransId, string CommandType,string methodType)
        {
            string JsonXMLResponse = string.Empty;
            String status = string.Empty;
            FCTrans objMPg = new FCTrans();
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                WebRequest myWebRequest = WebRequest.Create(Url);

                myWebRequest.ContentType = "application/x-www-form-urlencoded";
                //myWebRequest.ContentType = "application/json; charset=utf-8";
                myWebRequest.Timeout = 180000;
                if (methodType == "GET")
                {
                    myWebRequest.Method = methodType;
                }
                else if (methodType == "POST")
                {
                    myWebRequest.Method = methodType;
                    StreamWriter requestWriter = new StreamWriter(myWebRequest.GetRequestStream());
                    requestWriter.Write(PostString);
                    requestWriter.Close();
                }
                

                StreamReader responseReader = new StreamReader(myWebRequest.GetResponse().GetResponseStream());
                WebResponse myWebResponse = myWebRequest.GetResponse();
                Stream ReceiveStream = myWebResponse.GetResponseStream();
                Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
                StreamReader readStream = new StreamReader(ReceiveStream, encode);
                JsonXMLResponse = readStream.ReadToEnd();
                //"{\"amount\":\"2.00\",\"checksum\":\"1785c7d57546b430c8bfabf438ec9fb173bf3fc33c99ad09e77fecc84de04e84\",
                //\"errorCode\":null,\"errorMessage\":null,\"merchantTxnId\":\"FC0228133911941021\",\"status\":\"SUCCESS\",
                //\"txnId\":\"OAWaTMk3evwR7q_FC0228133911941021_1\"}"

                if (JsonXMLResponse.Contains("xml"))
                {
                    status = GetValueFromXml(JsonXMLResponse, "response_element");
                }
                else
                {
                    Newtonsoft.Json.Linq.JObject account = Newtonsoft.Json.Linq.JObject.Parse(JsonXMLResponse);
                    status = (string)account.SelectToken("status");//account.ToString();
                    
                }
                int i = objMPg.InsertWebServiceLog(MerchantTransId, CommandType, ToHash, Hashed, PostString, JsonXMLResponse, status, "", "");

            }
            catch (WebException ex)
            {

                int flag = objMPg.InsertExceptionLog("PaymentGateway", "srvPG", "GetPostReqResPayU", "Web Service Error", ex.Message, ex.StackTrace);
                int i = objMPg.InsertWebServiceLog(MerchantTransId, CommandType, ToHash, Hashed, PostString, JsonXMLResponse, status, ex.Message, ex.StackTrace);
            }
            return JsonXMLResponse;
        }

        public string result(string Userid, string Pwd, string Url, string Action, string Req, string methodType)
        {
            string strRes = "";
            try
            {

                string url = Url + Action;//"http://172.18.80.75/ITQAVAPI/api/user/GetBaggage/Indigo/DXB/"; // forbaggage

                if (methodType == "GET")
                {
                    url += Req;
                }
                else if (methodType == "POST")
                {

                }

                //ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                //WebRequest myWebRequest = WebRequest.Create(Url);
                //myWebRequest.Method = "POST";
                //myWebRequest.ContentType = "application/x-www-form-urlencoded";
                ////myWebRequest.ContentType = "application/json; charset=utf-8";
                //myWebRequest.Timeout = 180000;

                //StreamWriter requestWriter = new StreamWriter(myWebRequest.GetRequestStream());
                //requestWriter.Write(PostString);
                //requestWriter.Close();

                //StreamReader responseReader = new StreamReader(myWebRequest.GetResponse().GetResponseStream());
                //WebResponse myWebResponse = myWebRequest.GetResponse();
                //Stream ReceiveStream = myWebResponse.GetResponseStream();
                //Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
                //StreamReader readStream = new StreamReader(ReceiveStream, encode);
                //JsonXMLResponse = readStream.ReadToEnd();

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = methodType;
                request.Timeout = 300000;
                request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip");
                request.Accept = "application/xml";
                request.KeepAlive = false;
                string authInfo = Userid + ":" + Pwd;
                authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));
                request.Headers["Authorization"] = "Basic " + authInfo;

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        string message = String.Format("POST failed. Received HTTP {0}", response.StatusCode);
                        throw new ApplicationException(message);
                    }
                    else
                    {
                        // FlightSearchResults g = new FlightSearchResults();
                        Stream s = response.GetResponseStream();
                        if ((response.ContentEncoding.ToLower().Contains("gzip")))
                        {
                            s = new GZipStream(s, CompressionMode.Decompress);
                        }
                        //Stream s = response.GetResponseStream();
                        StreamReader rdr = new StreamReader(s);
                        strRes = rdr.ReadToEnd();
                    }
                }
            }
            catch (Exception ex)
            { }
            return strRes;
        }
        public string GetValueFromXml(string strRes, string ResType)
        {
            XmlDocument xd = new XmlDocument();
            xd.LoadXml(strRes);
            XDocument xdo = XDocument.Load(new XmlNodeReader(xd));
            string str = "";
            try
            {
                if (ResType.ToLower() == "response_element")
                {
                    //str = xdo.Descendants("responsecode").First().Value + "&";
                    str = xdo.Descendants("description").First().Value;
                }
                else
                { str = xdo.Descendants("responsecode").First().Value; }
            }
            catch (Exception ex)
            { }
            return str;
        }
    }
}
