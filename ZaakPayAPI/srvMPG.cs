﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace ZaakPayAPI
{
    public class srvMPG
    {
        public string GetPostReqResPayU(string PostString, string Url, string ToHash, string Hashed, string MerchantTransId, string CommandType)
        {
            string JsonXMLResponse = string.Empty;
            String status = string.Empty;
            MobikwikTrans objMPg = new MobikwikTrans();
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                WebRequest myWebRequest = WebRequest.Create(Url);
                myWebRequest.Method = "POST";
                myWebRequest.ContentType = "application/x-www-form-urlencoded";
                //myWebRequest.ContentType = "application/json; charset=utf-8";
                myWebRequest.Timeout = 180000;
                StreamWriter requestWriter = new StreamWriter(myWebRequest.GetRequestStream());
                requestWriter.Write(PostString);
                requestWriter.Close();
                StreamReader responseReader = new StreamReader(myWebRequest.GetResponse().GetResponseStream());
                WebResponse myWebResponse = myWebRequest.GetResponse();
                Stream ReceiveStream = myWebResponse.GetResponseStream();
                Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
                StreamReader readStream = new StreamReader(ReceiveStream, encode);
                JsonXMLResponse = readStream.ReadToEnd();
                if (JsonXMLResponse.Contains("xml"))
                {
                    status = GetValueFromXml(JsonXMLResponse, "response_element");
                }
                else
                {
                    Newtonsoft.Json.Linq.JObject account = Newtonsoft.Json.Linq.JObject.Parse(JsonXMLResponse);
                    status = (string)account.SelectToken("transaction_details." + MerchantTransId + ".status");
                }
                int i = objMPg.InsertWebServiceLog(MerchantTransId, CommandType, ToHash, Hashed, PostString, JsonXMLResponse, status, "", "");

            }
            catch (WebException ex)
            {

                int flag = objMPg.InsertExceptionLog("PaymentGateway", "srvPG", "GetPostReqResPayU", "Web Service Error", ex.Message, ex.StackTrace);
                int i = objMPg.InsertWebServiceLog(MerchantTransId, CommandType, ToHash, Hashed, PostString, JsonXMLResponse, status, ex.Message, ex.StackTrace);
            }
            return JsonXMLResponse;
        }
        public string GetValueFromXml(string strRes, string ResType)
        {
            XmlDocument xd = new XmlDocument();
            xd.LoadXml(strRes);
            XDocument xdo = XDocument.Load(new XmlNodeReader(xd));
            string str = "";
            try
            {
                if (ResType.ToLower() == "response_element")
                {
                    //str = xdo.Descendants("responsecode").First().Value + "&";
                    str = xdo.Descendants("description").First().Value;
                }
                else
                { str = xdo.Descendants("responsecode").First().Value; }
            }
            catch (Exception ex)
            { }
            return str;
        }
    }
}
