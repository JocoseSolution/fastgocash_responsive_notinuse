﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Threading.Tasks;
using System.Data.Common;
using System.Collections;
using EXCEPTION_LOG;

namespace COMN_DAL.Flight
{
    public class FlightDal
    {
        //SqlConnection con;
        //SqlCommand cmd;
        //SqlDataAdapter adap;
        private SqlDatabase DBHelper;

        public FlightDal(string connectionString)
        {
            DBHelper = new SqlDatabase(connectionString);
        }


        public DataSet GetFlightDetailsByOrderId(string orderId, string agentId)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand cmd = new SqlCommand();
                cmd.CommandText = "GetSelectedFltDtls_Gal";
                cmd.CommandType = CommandType.StoredProcedure;
                DBHelper.AddInParameter(cmd, "@TrackId", DbType.String, orderId);
                DBHelper.AddInParameter(cmd, "@UserId", DbType.String, agentId);

                ds = DBHelper.ExecuteDataSet(cmd);
            }
            catch (Exception ex)
            {
                ErrorLog.FileHandling("Flight", "Error_102", ex, "COMN_DAL.Flight.FlightDal.GetFlightDetailsByOrderId");

            }

            return ds;

        }


        public DataSet GetPaxDetailsByOrderId(string orderId)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand cmd = new SqlCommand();
                cmd.CommandText = "GetPaxDetails";
                cmd.CommandType = CommandType.StoredProcedure;
                DBHelper.AddInParameter(cmd, "@TrackId", DbType.String, orderId);

                ds = DBHelper.ExecuteDataSet(cmd);
            }
            catch (Exception ex)
            {
                ErrorLog.FileHandling("Flight", "Error_102", ex, "COMN_DAL.Flight.FlightDal.GetPaxDetailsByOrderId");

            }

            return ds;

        }

        public DataSet GetHdrDetails(string orderId)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand cmd = new SqlCommand();
                cmd.CommandText = "GetFltHdr";
                cmd.CommandType = CommandType.StoredProcedure;
                DBHelper.AddInParameter(cmd, "@TrackId", DbType.String, orderId);

                ds = DBHelper.ExecuteDataSet(cmd);
            }
            catch (Exception ex)
            {
                ErrorLog.FileHandling("Flight", "Error_102", ex, "COMN_DAL.Flight.FlightDal.GetHdrDetails");

            }

            return ds;

        }

        public DataSet GetFltFareDtl(string orderId)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand cmd = new SqlCommand();
                cmd.CommandText = "GetFltFareDtl";
                cmd.CommandType = CommandType.StoredProcedure;
                DBHelper.AddInParameter(cmd, "@TrackId", DbType.String, orderId);

                ds = DBHelper.ExecuteDataSet(cmd);
            }
            catch (Exception ex)
            {
                ErrorLog.FileHandling("Flight", "Error_102", ex, "COMN_DAL.Flight.FlightDal.GetFltFareDtl");

            }

            return ds;

        }

        public DataSet GetTktCredentials_GAL(string vc, string trip,string CrdType)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand cmd = new SqlCommand();
                cmd.CommandText = "GetTicketingCrdGAL";
                cmd.CommandType = CommandType.StoredProcedure;
                DBHelper.AddInParameter(cmd, "@VC", DbType.String, vc);
                DBHelper.AddInParameter(cmd, "@trip", DbType.String, trip);
                DBHelper.AddInParameter(cmd, "@CrdType", DbType.String, CrdType);

                ds = DBHelper.ExecuteDataSet(cmd);
            }
            catch (Exception ex)
            {
                ErrorLog.FileHandling("Flight", "Error_102", ex, "COMN_DAL.Flight.FlightDal.GetTktCredentials_GAL");

            }

            return ds;

        }

        public int InsertGdsBkgLogs(string OrderId, Hashtable PnrHT)
        {
            int res = 0;
            try
            {
                DbCommand cmd = new SqlCommand();
                cmd.CommandText = "InsertGdsBkgLogs";
                cmd.CommandType = CommandType.StoredProcedure;
                DBHelper.AddInParameter(cmd, "@OrderId", DbType.String, OrderId);
                DBHelper.AddInParameter(cmd, "@Pnr", DbType.String, PnrHT["ADTPNR"] + "," + PnrHT["CHDPNR"]);
                DBHelper.AddInParameter(cmd, "@SSReq", DbType.String, PnrHT["SSReq"]);
                DBHelper.AddInParameter(cmd, "@SSRes", DbType.String, PnrHT["SSRes"]);
                DBHelper.AddInParameter(cmd, "@PNBFReq1", DbType.String, PnrHT["PNBF1Req"]);
                DBHelper.AddInParameter(cmd, "@PNBFRes1", DbType.String, PnrHT["PNBF1Res"]);
                DBHelper.AddInParameter(cmd, "@PNBFReq2", DbType.String, PnrHT["PNBF2Req"]);
                DBHelper.AddInParameter(cmd, "@PNBFRes2", DbType.String, PnrHT["PNBF2Res"]);
                DBHelper.AddInParameter(cmd, "@PNBFReq3", DbType.String, PnrHT["PNBF3Req"]);
                DBHelper.AddInParameter(cmd, "@PNBFRes3", DbType.String, PnrHT["PNBF3Res"]);
                DBHelper.AddInParameter(cmd, "@SEReq", DbType.String, PnrHT["SEReq"]);
                DBHelper.AddInParameter(cmd, "@SERes", DbType.String, PnrHT["SERes"]);
                DBHelper.AddInParameter(cmd, "@Other", DbType.String, PnrHT["Other"]);
                res = DBHelper.ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                ErrorLog.FileHandling("Flight", "Error_102", ex, "COMN_DAL.Flight.FlightDal.InsertGdsBkgLogs");

            }
            return res;

        }

        public string GetGDSServiceCodeForPNRCreation(string vc, string trip)
        {
            string sericeCode = "";
            try
            {
                DbCommand cmd = new SqlCommand();
                cmd.CommandText = "SP_SERVICEPROVIDER_ENABLE";
                cmd.CommandType = CommandType.StoredProcedure;
                DBHelper.AddInParameter(cmd, "@VC", DbType.String, vc);
                DBHelper.AddInParameter(cmd, "@Trip", DbType.String, trip);

                sericeCode = Convert.ToString(DBHelper.ExecuteScalar(cmd));
            }
            catch (Exception ex)
            {
                ErrorLog.FileHandling("Flight", "Error_102", ex, "COMN_DAL.Flight.FlightDal.GetGDSServiceCodeForPNRCreation");

            }

            return sericeCode;

        }


        public int Ledgerandcreditlimit_Transaction_Insert(string AGENTID, double TOTALAFETRDIS, string TRACKID, string VC, string GDSPNR, string AGENCYNAME, string IP, string ProjectId, string BookedBy, string BillNo, string AvailBal, string EasyID, string status = "PreConfirm")
        {

            int resp = 0;

            try
            {
                DbCommand cmd = new SqlCommand();
                cmd.CommandText = "SP_INSERT_LEDGERNEWREGES_TRANSACTION";
                cmd.CommandType = CommandType.StoredProcedure;

                DBHelper.AddInParameter(cmd, "@AGENTID", DbType.String, AGENTID);
                DBHelper.AddInParameter(cmd, "@TOTALAFETRDIS", DbType.String, TOTALAFETRDIS);
                DBHelper.AddInParameter(cmd, "@TRACKID", DbType.String, TRACKID);
                DBHelper.AddInParameter(cmd, "@VC", DbType.String, VC);
                DBHelper.AddInParameter(cmd, "@GDSPNR", DbType.String, GDSPNR);
                DBHelper.AddInParameter(cmd, "@AGENCYNAME", DbType.String, AGENCYNAME);
                DBHelper.AddInParameter(cmd, "@IP", DbType.String, IP);
                DBHelper.AddInParameter(cmd, "@ProjectId", DbType.String, ProjectId);
                DBHelper.AddInParameter(cmd, "@BookedBy", DbType.String, BookedBy);
                DBHelper.AddInParameter(cmd, "@BillNo", DbType.String, BillNo);
                DBHelper.AddInParameter(cmd, "@AVAILBAL", DbType.String, AvailBal);
                DBHelper.AddInParameter(cmd, "@EasyID", DbType.String, EasyID);
                DBHelper.AddInParameter(cmd, "@Status", DbType.String, status);

                resp = Convert.ToInt16(DBHelper.ExecuteNonQuery(cmd));
                //End Ledger and Credit Limit With Transaction
            }
            catch (Exception ex)
            {
                ErrorLog.FileHandling("Flight", "Error_102", ex, "COMN_DAL.Flight.FlightDal.Ledgerandcreditlimit_Transaction_Insert");

            }

            return resp;
        }

        public DataSet GetAgencyDetails(string UserId)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand cmd = new SqlCommand();
                cmd.CommandText = "AgencyDetails";
                cmd.CommandType = CommandType.StoredProcedure;
                DBHelper.AddInParameter(cmd, "@UserId", DbType.String, UserId);
                ds = DBHelper.ExecuteDataSet(cmd);
            }
            catch (Exception ex)
            {
                ErrorLog.FileHandling("Flight", "Error_102", ex, "COMN_DAL.Flight.FlightDal.GetAgencyDetails");

            }
            return ds;
        }


        public int UpdateFltHeader(string trackid, string agname, string gdspnr, string airpnr, string status)
        {

            int resp = 0;
            try
            {
                DbCommand cmd = new SqlCommand();
                cmd.CommandText = "UpdateFltHeader";
                cmd.CommandType = CommandType.StoredProcedure;
                DBHelper.AddInParameter(cmd, "@OrderId", DbType.String, trackid);
                DBHelper.AddInParameter(cmd, "@Status", DbType.String, status);
                DBHelper.AddInParameter(cmd, "@AgName", DbType.String, agname);
                DBHelper.AddInParameter(cmd, "@GdsPnr", DbType.String, gdspnr);
                DBHelper.AddInParameter(cmd, "@AirlinePnr", DbType.String, airpnr);
                resp = Convert.ToInt16(DBHelper.ExecuteNonQuery(cmd));
            }
            catch (Exception ex)
            {
                ErrorLog.FileHandling("Flight", "Error_102", ex, "COMN_DAL.Flight.FlightDal.UpdateFltHeader");

            }
            return resp;


        }

        public int UpdateHoldBookingCharge(string OrderIDO, string holdBokingStatusO, decimal holdBookingChargeO, string OrderIDR, string holdBokingStatusR, decimal holdBookingChargeR)
        {

            int resp = 0;
            try
            {
                DbCommand cmd = new SqlCommand();
                cmd.CommandText = "Usp_UpdateHoldBookingCharge";
                cmd.CommandType = CommandType.StoredProcedure;
                DBHelper.AddInParameter(cmd, "@OrderIDO", DbType.String, OrderIDO);
                DBHelper.AddInParameter(cmd, "@HoldBokingStatusO", DbType.String, holdBokingStatusO);
                DBHelper.AddInParameter(cmd, "@HoldBookingChargeO", DbType.Decimal, holdBookingChargeO);
                DBHelper.AddInParameter(cmd, "@OrderIDR", DbType.String, OrderIDR);
                DBHelper.AddInParameter(cmd, "@HoldBokingStatusR", DbType.String, holdBokingStatusR);
                DBHelper.AddInParameter(cmd, "@HoldBookingChargeR", DbType.Decimal, holdBookingChargeR);
                resp = Convert.ToInt16(DBHelper.ExecuteNonQuery(cmd));
            }
            catch (Exception ex)
            {
                ErrorLog.FileHandling("Flight", "Error_102", ex, "COMN_DAL.Flight.FlightDal.UpdateHoldBookingCharge");

            }
            return resp;


        }

      


        public DataSet GetFltHoldBookingCharge(string GroupType, string agentId, string aircode, string tripType)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand cmd = new SqlCommand();
                cmd.CommandText = "SP_GET_FltHoldBookingCharges";
                cmd.CommandType = CommandType.StoredProcedure;
                DBHelper.AddInParameter(cmd, "@GroupType", DbType.String, GroupType);
                DBHelper.AddInParameter(cmd, "@AgentId", DbType.String, agentId);
                DBHelper.AddInParameter(cmd, "@AirCode", DbType.String, aircode);
                DBHelper.AddInParameter(cmd, "@TripType", DbType.String, tripType);
                ds = DBHelper.ExecuteDataSet(cmd);

            }
            catch (Exception ex)
            {
                ErrorLog.FileHandling("Flight", "Error_102", ex, "COMN_DAL.Flight.FlightDal.GetFltHoldBookingCharge");

            }
            return ds;
        }


    }
}
