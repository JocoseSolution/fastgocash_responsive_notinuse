﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class aeps_callback_webhook : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
    private SqlTransactionDom STDom = new SqlTransactionDom();
    private SqlDataAdapter adap;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string ipay_id = Request.QueryString["ipay_id"] != null ? Request.QueryString["ipay_id"].ToString() : string.Empty;
            string agent_id = Request.QueryString["agent_id"] != null ? Request.QueryString["agent_id"].ToString() : string.Empty;
            string opr_id = Request.QueryString["opr_id"] != null ? Request.QueryString["opr_id"].ToString() : string.Empty;
            string status = Request.QueryString["status"] != null ? Request.QueryString["status"].ToString() : string.Empty;
            string res_code = Request.QueryString["res_code"] != null ? Request.QueryString["res_code"].ToString() : string.Empty;
            string res_msg = Request.QueryString["res_msg"] != null ? Request.QueryString["res_msg"].ToString() : string.Empty;
            string txn_mode = Request.QueryString["txn_mode"] != null ? Request.QueryString["txn_mode"].ToString() : string.Empty;
            if (!string.IsNullOrEmpty(ipay_id.Trim()))
            {
                InsertQueryStrValues(ipay_id, agent_id, opr_id, status, res_code, res_msg, txn_mode);
            }
        }
    }

    private void InsertQueryStrValues(string ipay_id, string agent_id, string opr_id, string status, string res_code, string res_msg, string txn_mode)
    {
        try
        {
            SqlCommand cmd = new SqlCommand("sp_Callback_Webhook", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("ipay_id", ipay_id);
            cmd.Parameters.AddWithValue("agent_id", agent_id);
            cmd.Parameters.AddWithValue("opr_id", opr_id);
            cmd.Parameters.AddWithValue("status", status);
            cmd.Parameters.AddWithValue("res_code", res_code);
            cmd.Parameters.AddWithValue("res_msg", res_msg);
            cmd.Parameters.AddWithValue("txn_mode", txn_mode);

            con.Open();
            int k = cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
    }
}