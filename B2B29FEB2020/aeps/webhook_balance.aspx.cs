﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class aeps_webhook_balance : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string strReq = Request.Form.ToString();

        WebHookReqSave(strReq);
    }

    [WebMethod]
    public  void WebHookReqSave(string strReq)
    {
        try
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
            SqlDataAdapter adp = new SqlDataAdapter();
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            string strkk = "";
            string[] keys = Request.Form.AllKeys;
            for (int i = 0; i < keys.Length; i++)
            {
                strkk = strkk + keys[i] + ": " + Request.Form[keys[i]] + "<br>";
                //Response.Write(keys[i] + ": " + Request.Form[keys[i]] + "<br>");
            }
            string input;
            using (var reader = new StreamReader(Request.InputStream))
            {
                input = reader.ReadToEnd();
            }

            cmd = new SqlCommand("insert into T_AEPSWebhook (balance_req) values ('" + strkk+"aasASasaSas"+ input + "')", con);
            cmd.CommandType = CommandType.Text;
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
            con.Open();
            cmd.ExecuteNonQuery();
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
    }
}