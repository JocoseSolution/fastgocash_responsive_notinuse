﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GALWS.Travarena
{
    
    public class TravarenaHopingFlt
    {
        public string Origin { get; set; }
        public string Destination { get; set; }
        public string FlightTime { get; set; }
        public string TravelTime { get; set; }
        public string Equipment { get; set; }
        public string OriginTerminal { get; set; }
        public string DestinationTerminal { get; set; }
    }
}
