﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using STD.Shared;
using System.Data.Common;
using System.Collections;
using ITZERRORLOG;
using BAL;
//using ExceptionLog;
namespace STD.DAL
{
    public class FlightCommonDAL
    {
        SqlConnection con;
        SqlCommand cmd;
        SqlDataAdapter adap;
        // private string connectionStr;
        private SqlDatabase DBHelper;

        public FlightCommonDAL(string connectionString)
        {
            DBHelper = new SqlDatabase(connectionString);
        }

        //public FlightCommonDAL()//string SPRDISTR
        //{
        //    con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
        //    //connectionStr = SPRDISTR;
        //    adap = new SqlDataAdapter();
        //    DBHelper = new SqlDatabase(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
        //}

        //        public int InsertFltHeaderDetails(FltHeaderDetails ObjFHD)
        //        {
        //            int temp = 0;
        //            try
        //            {
        //                con.Open();
        //                cmd = new SqlCommand("SP_DIST_FLIGHT_INSERT_FLTHEADER", con);
        //                cmd.CommandType = CommandType.StoredProcedure;
        //                cmd.Parameters.AddWithValue("@ORDERID", ObjFHD.ORDERID);
        //                cmd.Parameters.AddWithValue("@SECTOR", ObjFHD.SECTOR);
        //                cmd.Parameters.AddWithValue("@STATUS", ObjFHD.STATUS);
        //                cmd.Parameters.AddWithValue("@MORDIFYSTATUS", ObjFHD.MORDIFYSTATUS);
        //                cmd.Parameters.AddWithValue("@GDSPNR", ObjFHD.GDSPNR);
        //                cmd.Parameters.AddWithValue("@AIRLINEPNR", ObjFHD.AIRLINEPNR);
        //                cmd.Parameters.AddWithValue("@VC", ObjFHD.VC);
        //                cmd.Parameters.AddWithValue("@DURATION", ObjFHD.DURATION);
        //                cmd.Parameters.AddWithValue("@TRIPTYPE", ObjFHD.TRIPTYPE);
        //                cmd.Parameters.AddWithValue("@TRIP", ObjFHD.TRIP);
        //                cmd.Parameters.AddWithValue("@TOURCODE", ObjFHD.TOURCODE);
        //                cmd.Parameters.AddWithValue("@TA_TOTALBOOKINGCOST", ObjFHD.TA_TOTALBOOKINGCOST);
        //                cmd.Parameters.AddWithValue("@DI_TOTALBOOKINGCOST", ObjFHD.DI_TOTALBOOKINGCOST);
        //                cmd.Parameters.AddWithValue("@TA_TOTALAFTERDIS", ObjFHD.TA_TOTALAFTERDIS);
        //                cmd.Parameters.AddWithValue("@DI_TOTALAFTERDIS", ObjFHD.DI_TOTALAFTERDIS);
        //                cmd.Parameters.AddWithValue("@SFDIS", ObjFHD.SFDIS);
        //                cmd.Parameters.AddWithValue("@ADDITIONALMARKUP", ObjFHD.ADDITIONALMARKUP);
        //                cmd.Parameters.AddWithValue("@ADULT", ObjFHD.ADULT);
        //                cmd.Parameters.AddWithValue("@CHILD", ObjFHD.CHILD);
        //                cmd.Parameters.AddWithValue("@INFANT", ObjFHD.INFANT);
        //                cmd.Parameters.AddWithValue("@AGENTID", ObjFHD.AGENTID);
        //                cmd.Parameters.AddWithValue("@AGENCYNAME", ObjFHD.AGENCYNAME);
        //                cmd.Parameters.AddWithValue("@AGENCYTYPE", ObjFHD.AGENCYTYPE);
        //                cmd.Parameters.AddWithValue("@PROVIDER", ObjFHD.PROVIDER);
        //                cmd.Parameters.AddWithValue("@DISTRID", ObjFHD.DISTRID);
        //                cmd.Parameters.AddWithValue("@EXECUTIVEID", ObjFHD.EXECUTIVEID);
        //                cmd.Parameters.AddWithValue("@PAYMENTTYPE", ObjFHD.PAYMENTTYPE);
        //                cmd.Parameters.AddWithValue("@PGTITLE", ObjFHD.PGTITLE);
        //                cmd.Parameters.AddWithValue("@PGFNAME", ObjFHD.PGFNAME);
        //                cmd.Parameters.AddWithValue("@PGLNAME", ObjFHD.PGLNAME);
        //                cmd.Parameters.AddWithValue("@PGMOBILE", ObjFHD.PGMOBILE);
        //                cmd.Parameters.AddWithValue("@PGEMAIL", ObjFHD.PGEMAIL);
        //                cmd.Parameters.AddWithValue("@REMARK", ObjFHD.REMARK);
        //                cmd.Parameters.AddWithValue("@REJECTEDREMARK", ObjFHD.REJECTEDREMARK);
        //                cmd.Parameters.AddWithValue("@RESUID", ObjFHD.RESUID); //Added on 25/7/2012 by Manish
        //                cmd.Parameters.AddWithValue("@RESUCHARGE", ObjFHD.RESUCHARGE); //Added on 25/7/2012 by Manish
        //                cmd.Parameters.AddWithValue("@RESUSERVICECHARGE", ObjFHD.RESUSERVICECHARGE); //Added on 25/7/2012 by Manish
        //                cmd.Parameters.AddWithValue("@RESUFAREDIFF", ObjFHD.RESUFAREDIFF); //Added on 25/7/2012 by Manish
        //                cmd.Parameters.AddWithValue("@PAXID", ObjFHD.PAXID);
        //                cmd.Parameters.AddWithValue("@IMPORTCHARGE", ObjFHD.IMPORTCHARGE);
        //                cmd.Parameters.AddWithValue("@ADDISCPRCNT", ObjFHD.ADDISCPRCNT);
        //                cmd.Parameters.AddWithValue("@ADDISCPRCNTON", ObjFHD.ADDISCPRCNTON);
        //                cmd.Parameters.AddWithValue("@DISTRDISCPRCNT", ObjFHD.DISTRDISCPRCNT);
        //                cmd.Parameters.AddWithValue("@DISTRISCPRCNTON", ObjFHD.DISTRISCPRCNTON);
        //                temp = cmd.ExecuteNonQuery();

        //            }
        //            catch (Exception ex)
        //            {
        //               // ExceptionLogger.FileHandling("FlightSearchService", "Err_001", ex, "FlightSearch");
        //            }
        //            finally
        //            {
        //                con.Close();
        //                cmd.Dispose();
        //            }
        //            return temp;
        //        }
        //        public int InsertFltDetails(FltDetails ObjFD)
        //        {
        //            int temp = 0;
        //            try
        //            {
        //                con.Open();
        //                cmd = new SqlCommand("SP_DIST_FLIGHT_INSERT_FLTDETAILS", con);
        //                cmd.CommandType = CommandType.StoredProcedure;
        //                cmd.Parameters.AddWithValue("@ORDERID", ObjFD.ORDERID);
        //                cmd.Parameters.AddWithValue("@DEPCITYORAIRPORTCODE", ObjFD.DEPCITYORAIRPORTCODE);
        //                cmd.Parameters.AddWithValue("@DEPCITYORAIRPORTNAME", ObjFD.DEPCITYORAIRPORTNAME);
        //                cmd.Parameters.AddWithValue("@ARRCITYORAIRPORTCODE", ObjFD.ARRCITYORAIRPORTCODE);
        //                cmd.Parameters.AddWithValue("@ARRCITYORAIRPORTNAME", ObjFD.ARRCITYORAIRPORTNAME);
        //                cmd.Parameters.AddWithValue("@DEPDATE", ObjFD.DEPDATE);
        //                cmd.Parameters.AddWithValue("@DEPTIME", ObjFD.DEPTIME);
        //                cmd.Parameters.AddWithValue("@ARRDATE", ObjFD.ARRDATE);
        //                cmd.Parameters.AddWithValue("@ARRTIME", ObjFD.ARRTIME);
        //                cmd.Parameters.AddWithValue("@AIRLINECODE", ObjFD.AIRLINECODE);
        //                cmd.Parameters.AddWithValue("@AIRLINENAME", ObjFD.AIRLINENAME);
        //                cmd.Parameters.AddWithValue("@FLTNUMBER", ObjFD.FLTNUMBER);
        //                cmd.Parameters.AddWithValue("@AIRCRAFT", ObjFD.AIRCRAFT);
        //                cmd.Parameters.AddWithValue("@ADTFAREBASIS", ObjFD.ADTFAREBASIS);
        //                cmd.Parameters.AddWithValue("@CHDFAREBASIS", ObjFD.CHDFAREBASIS);
        //                cmd.Parameters.AddWithValue("@INFFAREBASIS", ObjFD.INFFAREBASIS);
        //                cmd.Parameters.AddWithValue("@ADTRBD", ObjFD.ADTRBD);
        //                cmd.Parameters.AddWithValue("@CHDRBD", ObjFD.CHDRBD);
        //                cmd.Parameters.AddWithValue("@INFRBD", ObjFD.INFRBD);
        //                cmd.Parameters.AddWithValue("@AVLSEAT", ObjFD.AVLSEAT);
        //                temp = cmd.ExecuteNonQuery();

        //            }
        //            catch (Exception ex)
        //            {
        ////                ExceptionLogger.FileHandling("FlightSearchService","Err_001", ex,"FlightSearch");
        //            }
        //            finally
        //            {
        //                con.Close();
        //                cmd.Dispose();
        //            }
        //            return temp; ;
        //        }
        //        public int InsertFltPxDetails(FltPaxDetails ObjFC)
        //        {
        //            try
        //            {
        //                con.Open();
        //                cmd = new SqlCommand("SP_DIST_FLIGHT_INSERT_FLTPAXDETAILS", con);
        //                cmd.CommandType = CommandType.StoredProcedure;
        //                cmd.Parameters.AddWithValue("@OrderID", ObjFC.OrderID);
        //                cmd.Parameters.AddWithValue("@Title", ObjFC.Title);
        //                cmd.Parameters.AddWithValue("@FName", ObjFC.FName);
        //                cmd.Parameters.AddWithValue("@MName", ObjFC.MName);
        //                cmd.Parameters.AddWithValue("@LName", ObjFC.LName);
        //                cmd.Parameters.AddWithValue("@PaxType", ObjFC.PaxType);
        //                cmd.Parameters.AddWithValue("@TicketNo", ObjFC.TicketNo);
        //                cmd.Parameters.AddWithValue("@DOB", ObjFC.DOB);
        //                cmd.Parameters.AddWithValue("@FFNumber", ObjFC.FFNumber);
        //                cmd.Parameters.AddWithValue("@FFAirline", ObjFC.FFAirline);
        //                cmd.Parameters.AddWithValue("@MealType", ObjFC.MealType);
        //                cmd.Parameters.AddWithValue("@SeatType", ObjFC.SeatType);
        //                cmd.Parameters.AddWithValue("@IsPrimary", ObjFC.IsPrimary);
        //                cmd.Parameters.AddWithValue("@InfAssociatePaxName", ObjFC.InfAssociatePaxName);
        //                int temp = 0;
        //                temp = Convert.ToInt32(cmd.ExecuteScalar());
        //                con.Close();
        //                return temp;
        //            }
        //            catch (Exception ex)
        //            {
        //               // ExceptionLogger.FileHandling("FlightSearchService", "Err_001", ex, "FlightSearch");
        //            }
        //            finally
        //            {
        //                con.Close();
        //                cmd.Dispose();
        //            }
        //            return 0;

        //        }
        //        public int InsertFltFareDetails(FltFareDetails ObjFFD)
        //        {
        //            int temp = 0;
        //            try
        //            {
        //                con.Open();
        //                cmd = new SqlCommand("SP_DIST_FLIGHT_INSERT_FLTFAREDETAILS", con);
        //                cmd.CommandType = CommandType.StoredProcedure;
        //                cmd.Parameters.AddWithValue("@ORDERID", ObjFFD.ORDERID);
        //                cmd.Parameters.AddWithValue("@BASEFARE", ObjFFD.BASEFARE);
        //                cmd.Parameters.AddWithValue("@YQ", ObjFFD.YQ);
        //                cmd.Parameters.AddWithValue("@YR", ObjFFD.YR);
        //                cmd.Parameters.AddWithValue("@WO", ObjFFD.WO);
        //                cmd.Parameters.AddWithValue("@OT", ObjFFD.OT);
        //                cmd.Parameters.AddWithValue("@TOTALTAX", ObjFFD.TOTALTAX);
        //                cmd.Parameters.AddWithValue("@TA_TOTALFARE", ObjFFD.TA_TOTALFARE);
        //                cmd.Parameters.AddWithValue("@DI_TOTALFARE", ObjFFD.DI_TOTALFARE);
        //                cmd.Parameters.AddWithValue("@DI_SERVICETAX", ObjFFD.DI_SERVICETAX);
        //                cmd.Parameters.AddWithValue("@TA_SERVICETAX", ObjFFD.TA_SERVICETAX);
        //                cmd.Parameters.AddWithValue("@DI_TRANFEE", ObjFFD.DI_TRANFEE);
        //                cmd.Parameters.AddWithValue("@TA_TRANFEE", ObjFFD.TA_TRANFEE);
        //                cmd.Parameters.AddWithValue("@AD_ADMINMRK", ObjFFD.AD_ADMINMRK);
        //                cmd.Parameters.AddWithValue("@TA_AGENTMRK", ObjFFD.TA_AGENTMRK);
        //                cmd.Parameters.AddWithValue("@DI_DISTRMRK", ObjFFD.DI_DISTRMRK);
        //                cmd.Parameters.AddWithValue("@DI_TOTALDISCOUNT", ObjFFD.DI_TOTALDISCOUNT);
        //                cmd.Parameters.AddWithValue("@TA_TOTALDISCOUNT", ObjFFD.TA_TOTALDISCOUNT);
        //                cmd.Parameters.AddWithValue("@DI_PLB", ObjFFD.DI_PLB);
        //                cmd.Parameters.AddWithValue("@TA_PLB", ObjFFD.TA_PLB);
        //                cmd.Parameters.AddWithValue("@DI_DISCOUNT", ObjFFD.DI_DISCOUNT);
        //                cmd.Parameters.AddWithValue("@TA_DISCOUNT", ObjFFD.TA_DISCOUNT);
        //                cmd.Parameters.AddWithValue("@DI_CASHBACK", ObjFFD.DI_CASHBACK);
        //                cmd.Parameters.AddWithValue("@TA_CASHBACK", ObjFFD.TA_CASHBACK);
        //                cmd.Parameters.AddWithValue("@DI_TDS", ObjFFD.DI_TDS);
        //                cmd.Parameters.AddWithValue("@TA_TDS", ObjFFD.TA_TDS);
        //                cmd.Parameters.AddWithValue("@DI_TDSON", ObjFFD.DI_TDSON);
        //                cmd.Parameters.AddWithValue("@TA_TDSON", ObjFFD.TA_TDSON);
        //                cmd.Parameters.AddWithValue("@DI_TOTALAFTERDIS", ObjFFD.DI_TOTALAFTERDIS);
        //                cmd.Parameters.AddWithValue("@TA_TOTALAFTERDIS", ObjFFD.TA_TOTALAFTERDIS);
        //                cmd.Parameters.AddWithValue("@DI_AVLBALANCE", ObjFFD.DI_AVLBALANCE);
        //                cmd.Parameters.AddWithValue("@TA_AVLBALANCE", ObjFFD.TA_AVLBALANCE);
        //                cmd.Parameters.AddWithValue("@PAXTYPE", ObjFFD.PAXTYPE);
        //                cmd.Parameters.AddWithValue("@PAXID", ObjFFD.PAXID);               
        //                temp = cmd.ExecuteNonQuery();

        //            }
        //            catch (Exception ex)
        //            {
        //               // ExceptionLogger.FileHandling("FlightSearchService", "Err_001", ex, "FlightSearch");
        //            }
        //            finally
        //            {
        //                con.Close();
        //                cmd.Dispose();
        //            }
        //            return temp; ;
        //        }

        //        public int Insert_GDS_LOG(string VC, string ORDERID, string SELL_REQ, string SELL_RES, string ADDMULTI_REQ, string ADDMULTI_RES, string PRC_REQ, string PRC_RES, string TST_REQ, string TST_RES, string PNR_REQ, string PNR_RES, string OTHERS)
        //        {
        //            int temp = 0;
        //            try
        //            {
        //                cmd = new SqlCommand("SP_DIST_INSERT_GDS_BKG_XMLLOG", con);
        //                cmd.CommandType = CommandType.StoredProcedure;
        //                cmd.Parameters.AddWithValue("@VC",VC);
        //                cmd.Parameters.AddWithValue("@ORDERID", ORDERID);
        //                cmd.Parameters.AddWithValue("@SELL_REQ", SELL_REQ);
        //                cmd.Parameters.AddWithValue("@SELL_RES", SELL_RES);
        //                cmd.Parameters.AddWithValue("@ADDMULTI_REQ", ADDMULTI_REQ);
        //                cmd.Parameters.AddWithValue("@ADDMULTI_RES", ADDMULTI_RES);
        //                cmd.Parameters.AddWithValue("@PRC_REQ", PRC_REQ);
        //                cmd.Parameters.AddWithValue("@PRC_RES", PRC_RES);
        //                cmd.Parameters.AddWithValue("@TST_REQ", TST_REQ);
        //                cmd.Parameters.AddWithValue("@TST_RES", TST_RES);
        //                cmd.Parameters.AddWithValue("@PNR_REQ", PNR_REQ);
        //                cmd.Parameters.AddWithValue("@PNR_RES", PNR_RES);
        //                cmd.Parameters.AddWithValue("@OTHERS", OTHERS);
        //                temp = cmd.ExecuteNonQuery();
        //                con.Close();
        //                return temp;
        //            }
        //            catch (Exception ex)
        //            {
        //                //ExceptionLogger.FileHandling("FlightSearchService", "Err_001", ex, "FlightBooking");
        //            }
        //            finally
        //            {
        //                con.Close();
        //                cmd.Dispose();
        //            }
        //            return temp; ;
        //        }

        //        public int Insert_LCC_LOG(string VC, string ORDERID, string GDSPNR, string BOOKREQ, string BOOKRES, string ADDPAYREQ, string ADDPAYRES, string PRC_RES, string CONFIRMPAYRES)
        //        {
        //            int temp = 0;
        //            try
        //            {
        //                con.Open();
        //                cmd = new SqlCommand("SP_DIST_INSERT_LCC_BKG_XMLLOG", con);
        //                cmd.CommandType = CommandType.StoredProcedure;
        //                cmd.Parameters.AddWithValue("@VC", VC);
        //                cmd.Parameters.AddWithValue("@ORDERID", ORDERID);
        //                cmd.Parameters.AddWithValue("@GDSPNR", GDSPNR);
        //                cmd.Parameters.AddWithValue("@BOOKREQ", BOOKREQ);
        //                cmd.Parameters.AddWithValue("@BOOKRES", BOOKRES);
        //                cmd.Parameters.AddWithValue("@ADDPAYRES", ADDPAYREQ);
        //                cmd.Parameters.AddWithValue("@ADDPAYRES", ADDPAYRES);
        //                cmd.Parameters.AddWithValue("@CONFIRMPAYRES", CONFIRMPAYRES);
        //                temp = cmd.ExecuteNonQuery();
        //                con.Close();
        //            }
        //            catch (Exception ex)
        //            {
        //                //ExceptionLogger.FileHandling("FlightSearchService", "Err_001", ex, "FlightBooking");
        //            }
        //            finally
        //            {
        //                con.Close();
        //                cmd.Dispose();
        //            }
        //            return temp;
        //        }

        //        public DataTable Fetch_ALL_FltDetails(string tid, string Table)
        //        {
        //            DataTable MyDataTable = new DataTable();
        //            try
        //            {
        //                con.Open();
        //                cmd = new SqlCommand("SP_DIST_FLT_SELECT_ALLDETAILS", con);
        //                cmd.CommandType = CommandType.StoredProcedure;
        //                cmd.Parameters.AddWithValue("@ORDERID", tid);
        //                cmd.Parameters.AddWithValue("@TBLNAME", Table);
        //                SqlDataReader reader = cmd.ExecuteReader();
        //                MyDataTable.Load(reader);

        //            }
        //            catch (Exception ex)
        //            {
        //                //ExceptionLogger.FileHandling("FlightSearchService", "Err_001", ex, "FlightBooking");
        //            }
        //            finally
        //            {
        //                con.Close();
        //                cmd.Dispose();
        //            }
        //            return MyDataTable;
        //        }

        //        public int Update_FltHeader(string OrderId, String OwnerID, string GdsPNR, String AirlinePnr, string Status)
        //        {
        //            int temp = 0;
        //            try
        //            {
        //                con.Open();
        //                cmd = new SqlCommand("SP_DIST_FLT_UPDATE_FLTHEADER", con);
        //                cmd.CommandType = CommandType.StoredProcedure;
        //                cmd.Parameters.AddWithValue("@ORDERID", OrderId);
        //                cmd.Parameters.AddWithValue("@OWNERID", OwnerID);
        //                cmd.Parameters.AddWithValue("@GDSPNR", GdsPNR);
        //                cmd.Parameters.AddWithValue("@AIRLINEPNR", AirlinePnr);
        //                cmd.Parameters.AddWithValue("@BKGSTATUS", Status);
        //                temp = cmd.ExecuteNonQuery();
        //            }
        //            catch (Exception ex)
        //            {
        //                //ExceptionLogger.FileHandling("FlightSearchService", "Err_001", ex, "FlightBooking");
        //            }
        //            finally
        //            {
        //                con.Close();
        //                cmd.Dispose();
        //            }
        //            return temp;
        //        }

        //        public int Update_Flt_Pax(string OrderId, int PaxId, string TicketNo)
        //        {
        //            int temp = 0;
        //            try
        //            {
        //                con.Open();
        //                cmd = new SqlCommand("SP_DIST_FLT_UPDATE_PAXDETAILS", con);
        //                cmd.CommandType = CommandType.StoredProcedure;
        //                cmd.Parameters.AddWithValue("@ORDERID", OrderId);
        //                cmd.Parameters.AddWithValue("@PAXID", PaxId);
        //                cmd.Parameters.AddWithValue("@TICKETNUMBER", TicketNo);
        //                temp = cmd.ExecuteNonQuery();
        //            }
        //            catch (Exception ex)
        //            {
        //                //ExceptionLogger.FileHandling("FlightSearchService", "Err_001", ex, "FlightBooking");
        //            }
        //            finally
        //            {
        //                con.Close();
        //                cmd.Dispose();
        //            }
        //            return temp;
        //        }

        //        public DataTable Update_Dis_Agent_Limit(string OwnerId, double TAamount, double DIamount)
        //        {
        //            DataTable MyDataTable = new DataTable();
        //            try
        //            {
        //                con.Open();
        //                cmd = new SqlCommand("SP_DIST_AGENT_SUBTRACTFROMLIMIT", con);
        //                cmd.CommandType = CommandType.StoredProcedure;
        //                cmd.Parameters.AddWithValue("@OWNERID", OwnerId);
        //                cmd.Parameters.AddWithValue("@TA_AMOUNT", TAamount);
        //                cmd.Parameters.AddWithValue("@DI_AMOUNT", DIamount);
        //                SqlDataReader reader = cmd.ExecuteReader();
        //                MyDataTable.Load(reader);
        //                return MyDataTable;
        //            }
        //            catch (Exception ex)
        //            {
        //                //ExceptionLogger.FileHandling("FlightSearchService", "Err_001", ex, "");
        //            }
        //            finally
        //            {
        //                con.Close();
        //                cmd.Dispose();
        //            }
        //            return MyDataTable;
        //        }

        //        public DataTable SelectAgencyDetail(string AgentID, string trackid) //WHERE User_Id = '" + AgentID
        //        {
        //            con.Open();
        //            DataTable dt = new DataTable();//[SP_DIST_AGT_SELECT]
        //            //if ((trackid == "") && (AgentID != ""))
        //            //{
        //            if ((trackid != "") && (AgentID != ""))
        //            {
        //                adap = new SqlDataAdapter("SELECT AGENCYNAME,AGENCYLOGO,EMAIL,MOBILE,ADDRESS,(CITY+' - '+STATE+' - '+COUNTRY) AS ADDRESS1 from TBL_REGISTER_AGENT  WHERE USERID = '" + AgentID + "' ", con);
        //                adap.Fill(dt);
        //                // return dt;
        //            }
        //            else if ((AgentID == "") && (trackid != ""))
        //            {
        //                cmd = new SqlCommand("SP_AGENCY_DETAIL", con);
        //                cmd.CommandType = CommandType.StoredProcedure;
        //                cmd.Parameters.AddWithValue("@trackid", trackid);
        //                adap.SelectCommand = cmd;
        //                adap.Fill(dt);
        //                // return dt;
        //            }
        //            return dt;
        //        }

        //        /// <summary>
        //        /// Fetches Datatable Based on OrderId and tablename(FLT_Details,PaxDetails,etc)
        //        /// </summary>
        //        /// <param name="OrderId"></param>
        //        /// <param name="table"></param>
        //        /// <returns></returns>
        //        public DataTable SelectFlight_Details(string OrderId, string table)
        //        {
        //            DataTable dt = new DataTable();
        //            try
        //            {
        //                con.Open();
        //                cmd = new SqlCommand("SP_DIST_FLT_COMMON", con);
        //                cmd.CommandType = CommandType.StoredProcedure;
        //                cmd.Parameters.AddWithValue("@ORDERID", OrderId);
        //                cmd.Parameters.AddWithValue("@PROCTYPE", table);
        //                adap.SelectCommand = cmd;
        //                adap.Fill(dt);
        //            }
        //            catch (Exception ex)
        //            {
        //                //ExceptionLogger.FileHandling("FlightSearchService", "Err_001", ex, "FlightSearch");
        //            }
        //            finally
        //            {
        //                con.Close();
        //                cmd.Dispose();
        //            }
        //            return dt;
        //        }


        //        ///// <summary>
        //        ///// 
        //        ///// </summary>
        //        ///// <param name="jrnyType"></param>
        //        ///// <param name="City"></param>
        //        ///// <returns></returns>
        //        //public List<FlightCityList> FetchCityName(string jrnyType, string City)
        //        //{

        //        //    var listcity = new List<FlightCityList>();
        //        //    DbCommand cmd = new SqlCommand();
        //        //    cmd.CommandText = "SP_DIST_GET_CITYNAME";
        //        //    cmd.CommandType = CommandType.StoredProcedure;
        //        //    DBHelper.AddInParameter(cmd, "@Trip", DbType.String, jrnyType);
        //        //    DBHelper.AddInParameter(cmd, "@Code", DbType.String, City);
        //        //    IDataReader reader = DBHelper.ExecuteReader(cmd);

        //        //    while (reader.Read())
        //        //    {
        //        //        FlightCityList FCCity = new FlightCityList();
        //        //        FCCity.City = reader.GetString(reader.GetOrdinal("City"));
        //        //        FCCity.AirportCode = reader.GetString(reader.GetOrdinal("AirportCode"));
        //        //        FCCity.AirportName = reader.GetString(reader.GetOrdinal("AirportName"));
        //        //        FCCity.CountryCode = reader.GetString(reader.GetOrdinal("CountryCode"));
        //        //        listcity.Add(FCCity);
        //        //    }
        //        //    reader.Close();
        //        //    reader.Dispose();
        //        //    return listcity;

        //        //}
        //        ///// <summary>
        //        ///// 
        //        ///// </summary>
        //        ///// <param name="AirlineCode"></param>
        //        ///// <returns></returns>
        //        //public List<AirlineList> FetchAirlineName(string AirlineCode)
        //        //{

        //        //    var listairline = new List<AirlineList>();
        //        //    DbCommand cmd = new SqlCommand();
        //        //    cmd.CommandText = "SP_DIST_GET_AIRLINENAME";
        //        //    cmd.CommandType = CommandType.StoredProcedure;
        //        //    DBHelper.AddInParameter(cmd, "@CODE", DbType.String, AirlineCode);
        //        //    IDataReader reader = DBHelper.ExecuteReader(cmd);
        //        //    while (reader.Read())
        //        //    {
        //        //        AirlineList FC = new AirlineList();
        //        //        FC.AlilineName = reader.GetString(reader.GetOrdinal("AirlineName"));
        //        //        FC.AirlineCode = reader.GetString(reader.GetOrdinal("AirlineCode"));
        //        //        listairline.Add(FC);
        //        //    }
        //        //    reader.Close();
        //        //    reader.Dispose();
        //        //    return listairline;
        //        //}
        //        /// <summary>
        //        /// 
        //        /// </summary>
        //        /// <param name="AgentId"></param>
        //        /// <param name="DistrId"></param>
        //        /// <param name="UserType"></param>
        //        /// <param name="jrnyType"></param>
        //        /// <returns></returns>
        //        public DataSet GetMarkupDetails(string AgentId, string DistrId, string UserType, string jrnyType)
        //        {
        //            DbCommand cmd = new SqlCommand();
        //            cmd.CommandText = "SP_DIST_FLT_GETMARKUP";
        //            cmd.CommandType = CommandType.StoredProcedure;
        //            DBHelper.AddInParameter(cmd, "@AgentID", DbType.String, AgentId);
        //            DBHelper.AddInParameter(cmd, "@DistrID", DbType.String, DistrId);
        //            DBHelper.AddInParameter(cmd, "@UserType", DbType.String, UserType);
        //            DBHelper.AddInParameter(cmd, "@Trip", DbType.String, jrnyType);
        //            DataSet MrkDs = new DataSet();
        //            //MrkDs.Tables.Add("TBL_AIR_MRK_ADMIN");
        //            //MrkDs.Tables.Add("TBL_AIR_MRK_DIST");
        //            //MrkDs.Tables.Add("TBL_AIR_MRK_AGENT");
        //            MrkDs = DBHelper.ExecuteDataSet(cmd);
        //            if (MrkDs.Tables.Count == 1)
        //            {
        //                MrkDs.Tables[0].TableName = "TBL_AIR_MRK_ADMIN";
        //            }
        //            else if (MrkDs.Tables.Count == 2)
        //            {
        //                MrkDs.Tables[0].TableName = "TBL_AIR_MRK_ADMIN";
        //                MrkDs.Tables[1].TableName = "TBL_AIR_MRK_DIST";
        //            }
        //            else if (MrkDs.Tables.Count == 3)
        //            {
        //                MrkDs.Tables[0].TableName = "TBL_AIR_MRK_ADMIN";
        //                MrkDs.Tables[1].TableName = "TBL_AIR_MRK_DIST";
        //                MrkDs.Tables[2].TableName = "TBL_AIR_MRK_AGENT";
        //            }
        //            return MrkDs;
        //        }

        //        /// <summary>
        //        /// Returns True False based On DISTRID,AgentID,NetFare and Checks if Credit Limit is Greater then Fare
        //        /// </summary>
        //        /// <param name="OrderId"></param>
        //        /// <param name="table"></param>
        //        /// <returns></returns>
        //        public bool CHECK_AGENT_DISTR_CLIMIT(string AgentId, string DistrId, double Netfare)
        //        {
        //            bool Status=false;
        //            try
        //            {
        //                con.Open();
        //                SqlCommand cmd = new SqlCommand("SP_DIST_CHECK_BALANCE_AGENT_DISTR", con);
        //                cmd.CommandType = CommandType.StoredProcedure;
        //                cmd.Parameters.AddWithValue("@AGENTID", AgentId);
        //                cmd.Parameters.AddWithValue("@DISTRID", DistrId);
        //                cmd.Parameters.AddWithValue("@AMOUNT", Netfare);
        //                Status = Convert.ToBoolean(cmd.ExecuteScalar());
        //            }
        //            catch (Exception ex)
        //            {
        //                //ExceptionLogger.FileHandling("FlightSearchService", "Err_001", ex, "FlightBooking");
        //            }
        //            finally
        //            {
        //                con.Close();
        //            }
        //            return Status;
        //        }

        //        public bool CHECK_ORDERID(string ORDERID)
        //        {
        //            try
        //            {
        //                con.Open();
        //                SqlCommand cmd = new SqlCommand("CheckOrderId", con);
        //                cmd.CommandType = CommandType.StoredProcedure;
        //                cmd.Parameters.AddWithValue("@ORDERID", ORDERID);
        //                bool Status = Convert.ToBoolean(cmd.ExecuteScalar());
        //                return Status;
        //            }
        //            catch (Exception ex)
        //            {
        //                throw ex;
        //            }
        //            finally
        //            {
        //                con.Close();
        //            }

        //        }

        //        public List<GetFtlCommission> GetFtlCommission(GetFtlCommission objGetFC)
        //        {

        //            con.Open();
        //            var listcommission = new List<GetFtlCommission>();
        //            SqlCommand cmd = new SqlCommand("SP_DIST_FLT_GETCOMMISSION", con);
        //            cmd.CommandType = CommandType.StoredProcedure;
        //            cmd.Parameters.AddWithValue("@BaseFare", objGetFC.BaseFare);
        //            cmd.Parameters.AddWithValue("@YQ", objGetFC.YQ);
        //            cmd.Parameters.AddWithValue("@OWNERID", objGetFC.OwnerId);
        //            cmd.Parameters.AddWithValue("@VC", objGetFC.VC);
        //            cmd.Parameters.AddWithValue("@Trip", objGetFC.Trip);
        //            cmd.Parameters.AddWithValue("@PaxCnt", objGetFC.PaxCnt);



        //            //DbCommand cmd = new SqlCommand();
        //            //cmd.CommandText = "SP_DIST_FLT_GETCOMMISSION";
        //            //cmd.CommandType = CommandType.StoredProcedure;
        //            //DBHelper.AddInParameter(cmd, "@BaseFare", DbType.Double, objGetFC.BaseFare);
        //            //DBHelper.AddInParameter(cmd, "@YQ", DbType.Double, objGetFC.YQ);

        //            //DBHelper.AddInParameter(cmd, "@OWNERID", DbType.String , objGetFC.OwnerId );
        //            //DBHelper.AddInParameter(cmd, "@VC", DbType.Double, objGetFC.VC );
        //            //DBHelper.AddInParameter(cmd, "@Trip", DbType.Double, objGetFC.Trip);
        //            //DBHelper.AddInParameter(cmd, "@PaxCnt", DbType.Int32 , objGetFC.PaxCnt);

        //            IDataReader reader = cmd.ExecuteReader();
        //            while (reader.Read())
        //            {
        //                listcommission.Add(new GetFtlCommission
        //                {
        //                    ADCOMM = Convert.ToDouble(reader["ADCOMM"]),
        //                    ADCB = Convert.ToDouble(reader["ADCB"]),
        //                    DICOMM = Convert.ToDouble(reader["DICOMM"]),
        //                    DICB = Convert.ToDouble(reader["DICB"]),

        //                });
        //                //GetFtlCommission GFC = new GetFtlCommission();
        //                //GFC.ADCOMM = reader.GetDouble(reader.GetOrdinal("ADCOMM"));
        //                //GFC.ADCB = reader.GetDouble(reader.GetOrdinal("ADCB"));
        //                //GFC.DICOMM = reader.GetDouble(reader.GetOrdinal("DICOMM"));
        //                //GFC.DICB = reader.GetDouble(reader.GetOrdinal("DICB"));

        //                // listcommission.Add(GFC);
        //            }
        //            reader.Close();
        //            reader.Dispose();
        //            return listcommission;
        //        }

        //        public Hashtable CalcFltCommission(Double BaseFare, Double YQ, string OwnerId, string VC, string Trip, int PaxCnt, string Org, string Dst, string Cls)
        //        {
        //            Hashtable listcommission = new Hashtable();
        //            try
        //            {
        //                DbCommand DBCmd = new SqlCommand();
        //                DBCmd.CommandText = "SP_DIST_FLT_GETCOMMISSION";
        //                DBCmd.CommandType = CommandType.StoredProcedure;
        //                DBHelper.AddInParameter(DBCmd, "@BaseFare", DbType.Decimal, BaseFare);
        //                DBHelper.AddInParameter(DBCmd, "@YQ", DbType.Decimal, YQ);
        //                DBHelper.AddInParameter(DBCmd, "@OWNERID", DbType.String, OwnerId);
        //                DBHelper.AddInParameter(DBCmd, "@VC", DbType.String, VC);
        //                DBHelper.AddInParameter(DBCmd, "@PaxCnt", DbType.Int16, PaxCnt);
        //                DBHelper.AddInParameter(DBCmd, "@Trip", DbType.String, Trip);
        //                DBHelper.AddInParameter(DBCmd, "@Org", DbType.String, Org);
        //                DBHelper.AddInParameter(DBCmd, "@Dst", DbType.String, Dst);
        //                DBHelper.AddInParameter(DBCmd, "@Cls", DbType.String, Cls);
        //                IDataReader rd = DBHelper.ExecuteReader(DBCmd);
        //                while (rd.Read())
        //                {
        //                    listcommission.Add("ADCOMM", Convert.ToDouble(rd["ADCOMM"]));
        //                    listcommission.Add("ADCB", Convert.ToDouble(rd["ADCB"]));
        //                    listcommission.Add("DICOMM", Convert.ToDouble(rd["DICOMM"]));
        //                    listcommission.Add("DICB", Convert.ToDouble(rd["DICB"]));

        //                }
        //            }
        //            catch (Exception)
        //            {
        //            }
        //            finally
        //            {
        //            }
        //            return listcommission;
        //        }

        //        public List<GetFtlServiceTaxTFee> GetFtlSrvTaxTFee(GetFtlServiceTaxTFee objGetFSTT)
        //        {

        //            con.Open();
        //            var listTaxFee = new List<GetFtlServiceTaxTFee>();
        //            SqlCommand cmd = new SqlCommand("SP_DIST_FLT_CALC_SRVTAX", con);
        //            cmd.CommandType = CommandType.StoredProcedure;
        //            cmd.Parameters.AddWithValue("@VC", objGetFSTT.VC);
        //            cmd.Parameters.AddWithValue("@ADCOMM", objGetFSTT.ADCOMM);
        //            cmd.Parameters.AddWithValue("@DICOMM ", objGetFSTT.DICOMM);

        //            cmd.Parameters.AddWithValue("@BasicFare", objGetFSTT.BaseFare);
        //            cmd.Parameters.AddWithValue("@YQ", objGetFSTT.YQ);
        //            cmd.Parameters.AddWithValue("@TRIP", objGetFSTT.Trip);



        //            //DbCommand cmd = new SqlCommand();
        //            //cmd.CommandText = "SP_DIST_FLT_GETCOMMISSION";
        //            //cmd.CommandType = CommandType.StoredProcedure;
        //            //DBHelper.AddInParameter(cmd, "@BaseFare", DbType.Double, objGetFC.BaseFare);
        //            //DBHelper.AddInParameter(cmd, "@YQ", DbType.Double, objGetFC.YQ);

        //            //DBHelper.AddInParameter(cmd, "@OWNERID", DbType.String , objGetFC.OwnerId );
        //            //DBHelper.AddInParameter(cmd, "@VC", DbType.Double, objGetFC.VC );
        //            //DBHelper.AddInParameter(cmd, "@Trip", DbType.Double, objGetFC.Trip);
        //            //DBHelper.AddInParameter(cmd, "@PaxCnt", DbType.Int32 , objGetFC.PaxCnt);

        //            IDataReader reader = cmd.ExecuteReader();
        //            while (reader.Read())
        //            {
        //                listTaxFee.Add(new GetFtlServiceTaxTFee
        //                {
        //                    ADSTax = Convert.ToDouble(reader["ADSTax"]),
        //                    DISTax = Convert.ToDouble(reader["DISTax"]),
        //                    TFee = Convert.ToDouble(reader["TFee"]),
        //                    //DICB = Convert.ToDouble(reader["DICB"]),

        //                });
        //                //GetFtlCommission GFC = new GetFtlCommission();
        //                //GFC.ADCOMM = reader.GetDouble(reader.GetOrdinal("ADCOMM"));
        //                //GFC.ADCB = reader.GetDouble(reader.GetOrdinal("ADCB"));
        //                //GFC.DICOMM = reader.GetDouble(reader.GetOrdinal("DICOMM"));
        //                //GFC.DICB = reader.GetDouble(reader.GetOrdinal("DICB"));

        //                // listcommission.Add(GFC);
        //            }
        //            reader.Close();
        //            reader.Dispose();
        //            return listTaxFee;
        //        }

        //        public Hashtable CalcFltSrvTaxTFee(string VC, double ADCOMM, double DICOMM, double BasicFare, double YQ, string Trip)
        //        {

        //            Hashtable listTaxFee = new Hashtable();
        //            try
        //            {
        //                con.Open();
        //                cmd = new SqlCommand("SP_DIST_FLT_CALC_SRVTAX", con);
        //                cmd.CommandType = CommandType.StoredProcedure;
        //                cmd.Parameters.AddWithValue("@VC", VC);
        //                cmd.Parameters.AddWithValue("@ADCOMM", ADCOMM);
        //                cmd.Parameters.AddWithValue("@DICOMM ", DICOMM);
        //                cmd.Parameters.AddWithValue("@BasicFare", BasicFare);
        //                cmd.Parameters.AddWithValue("@YQ", YQ);
        //                cmd.Parameters.AddWithValue("@TRIP", Trip);

        //                IDataReader reader = cmd.ExecuteReader();
        //                while (reader.Read())
        //                {
        //                    listTaxFee.Add("ADSTax", Convert.ToDouble(reader["ADSTax"]));
        //                    listTaxFee.Add("DISTax", Convert.ToDouble(reader["DISTax"]));
        //                    listTaxFee.Add("TFee", Convert.ToDouble(reader["TFee"]));
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                //ExceptionLogger.FileHandling("FlightSearchService", "Err_001", ex, "FlightBooking");
        //            }
        //            finally
        //            {
        //                cmd.Dispose();
        //                con.Close();
        //            }
        //            return listTaxFee;
        //        }

        //        public List<GetTDS> GetTDS(GetTDS objGetTDS)
        //        {

        //            con.Open();
        //            var listTDS = new List<GetTDS>();
        //            SqlCommand cmd = new SqlCommand("SP_DIST_FLT_CALC_TDS", con);
        //            cmd.CommandType = CommandType.StoredProcedure;
        //            cmd.Parameters.AddWithValue("@DistrId", objGetTDS.DistrId);
        //            cmd.Parameters.AddWithValue("@AgentId", objGetTDS.AgentId);
        //            cmd.Parameters.AddWithValue("@UserType ", objGetTDS.UserType);

        //            cmd.Parameters.AddWithValue("@DistTdsOn", objGetTDS.DistTdsOn);
        //            cmd.Parameters.AddWithValue("@TdsOn", objGetTDS.TdsOn);
        //            IDataReader reader = cmd.ExecuteReader();
        //            while (reader.Read())
        //            {
        //                listTDS.Add(new GetTDS
        //                {
        //                    TdsPrcnt = reader["TdsPrcnt"].ToString(),
        //                    DTdsPrcnt = reader["DTdsPrcnt"].ToString(),

        //                    //DICB = Convert.ToDouble(reader["DICB"]),

        //                });
        //                //GetFtlCommission GFC = new GetFtlCommission();
        //                //GFC.ADCOMM = reader.GetDouble(reader.GetOrdinal("ADCOMM"));
        //                //GFC.ADCB = reader.GetDouble(reader.GetOrdinal("ADCB"));
        //                //GFC.DICOMM = reader.GetDouble(reader.GetOrdinal("DICOMM"));
        //                //GFC.DICB = reader.GetDouble(reader.GetOrdinal("DICB"));

        //                // listcommission.Add(GFC);
        //            }
        //            reader.Close();
        //            reader.Dispose();
        //            return listTDS;
        //        }

        //        public Hashtable CalcFltTDS(string OwnerId, double ADTdsOn, double DITdsOn)
        //        {
        //            Hashtable listTDS = new Hashtable();
        //            try
        //            {
        //                con.Open();
        //                cmd = new SqlCommand("SP_DIST_FLT_CALC_TDS", con);
        //                cmd.CommandType = CommandType.StoredProcedure;
        //                cmd.Parameters.AddWithValue("@OwnerId ", OwnerId);
        //                cmd.Parameters.AddWithValue("@ADTdsOn", ADTdsOn);
        //                cmd.Parameters.AddWithValue("@DITdsOn", DITdsOn);
        //                IDataReader reader = cmd.ExecuteReader();
        //                while (reader.Read())
        //                {
        //                    listTDS.Add("ADTds", Convert.ToDouble(reader["ADTds"].ToString()));
        //                    listTDS.Add("DITds", Convert.ToDouble(reader["DITds"].ToString()));
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //            }
        //            finally
        //            {
        //                cmd.Dispose();
        //                con.Close();
        //            }
        //            return listTDS;
        //        }
        //        //Fetch Fare Details by PaxID
        //        public List<GetFareDetailsByPaxId> GetFareDetailsByPaxId(int PaxId)
        //        {

        //            var listfare = new List<GetFareDetailsByPaxId>();
        //            DbCommand cmd = new SqlCommand();
        //            cmd.CommandText = "SP_DIST_GET_FLTFAREDETAILSBYPAXID";
        //            cmd.CommandType = CommandType.StoredProcedure;
        //            DBHelper.AddInParameter(cmd, "@PaxId", DbType.Int32, PaxId);
        //            IDataReader reader = DBHelper.ExecuteReader(cmd);
        //            while (reader.Read())
        //            {
        //                GetFareDetailsByPaxId GFDBP = new GetFareDetailsByPaxId();
        //                GFDBP.TA_TOTALAFTERDIS = Convert.ToDouble(reader.GetDecimal(reader.GetOrdinal("TA_TOTALAFTERDIS")));
        //                GFDBP.DI_TOTALAFTERDIS = Convert.ToDouble(reader.GetDecimal(reader.GetOrdinal("DI_TOTALAFTERDIS")));
        //                listfare.Add(GFDBP);
        //            }
        //            reader.Close();
        //            reader.Dispose();
        //            return listfare;
        //        }

        //        public Hashtable Check_Bkng_Tktng_Status(string UserId, string UserType, string OwnerId, double DI_Netfare, double TA_Netfare)
        //        {
        //            Hashtable Bkng_Stat = new Hashtable();
        //            try
        //            {
        //                con.Open();
        //                cmd = new SqlCommand("CheckAllowBooking", con);
        //                cmd.CommandType = CommandType.StoredProcedure;
        //                cmd.Parameters.AddWithValue("@OwnerId", OwnerId);
        //                cmd.Parameters.AddWithValue("@UserId ", UserId);
        //                cmd.Parameters.AddWithValue("@LoginUserType", UserType);
        //                cmd.Parameters.AddWithValue("@DINetFare ", DI_Netfare);
        //                cmd.Parameters.AddWithValue("@TANetFare", TA_Netfare);
        //                IDataReader reader = cmd.ExecuteReader();
        //                while (reader.Read())
        //                {
        //                    Bkng_Stat.Add("BKNGSTATUS", Convert.ToBoolean(reader["BKNGSTATUS"].ToString()));
        //                    Bkng_Stat.Add("TICKSTATUS", Convert.ToBoolean(reader["TICKSTATUS"].ToString()));
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                //ExceptionLogger.FileHandling("FlightSearchService", "Err_001", ex, "FlightBooking");
        //            }
        //            finally
        //            {
        //                cmd.Dispose();
        //                con.Close();
        //            }
        //            return Bkng_Stat;
        //        }

        public DataTable Get_TktCredentials(string VC)
        {
            try
            {
                con.Open();
                cmd = new SqlCommand("SP_DIST_FLT_SELECT_TICKETINGID", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@VC", VC);
                SqlDataReader reader = cmd.ExecuteReader();
                DataTable MyDataTable = new DataTable();
                MyDataTable.Load(reader);
                return MyDataTable;
            }
            catch
            {
                throw;
            }
            finally
            {
                con.Close();
                cmd.Dispose();
            }
        }
        //        //GET FLTHEADER DETAILS BY STATUS AND TRIP
        //        public DataTable GET_FLTHEADERDETAILSBYSTATUS(string STATUS, string TRIP, string MULTISTATUS, string EXECID)
        //        {
        //            DataTable dtflt = new DataTable();
        //            try
        //            {
        //                con.Open();
        //                cmd = new SqlCommand("SP_DIST_GET_FLTHEADERDETAILSBYSTATUS", con);
        //                cmd.CommandType = CommandType.StoredProcedure;
        //                cmd.Parameters.AddWithValue("@STATUS ", STATUS);
        //                cmd.Parameters.AddWithValue("@TRIP", TRIP);
        //                cmd.Parameters.AddWithValue("@MULTISTATUS", MULTISTATUS);
        //                cmd.Parameters.AddWithValue("@EXECID", EXECID);
        //                SqlDataReader reader = cmd.ExecuteReader();

        //                dtflt.Load(reader);

        //            }
        //            catch (Exception ex)
        //            {
        //            }
        //            finally
        //            {
        //                cmd.Dispose();
        //                con.Close();
        //            }
        //            return dtflt;
        //        }

        //public int UpdateFltDetails(string ORDERID, string EXECID, string STATUS, string REJECTRM, string GDSPNR, string AIRLINEPNR)
        //{
        //    try
        //    {
        //        con.Open();
        //        SqlCommand cmd = new SqlCommand("SP_DIST_FLT_UPDATE_HEADERDETAILS", con);
        //        cmd.CommandType = CommandType.StoredProcedure;
        //        cmd.Parameters.AddWithValue("@ORDERID", ORDERID);
        //        cmd.Parameters.AddWithValue("@EXECID", EXECID);
        //        cmd.Parameters.AddWithValue("@STATUS", STATUS);
        //        cmd.Parameters.AddWithValue("@REJECTRM", REJECTRM);
        //        cmd.Parameters.AddWithValue("@GDSPNR", GDSPNR);
        //        cmd.Parameters.AddWithValue("@AIRLINEPNR", AIRLINEPNR);
        //        int temp = 0;
        //        temp = cmd.ExecuteNonQuery();
        //        con.Close();
        //        return temp;
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine(ex.Message);
        //    }
        //    return 0;
        //}

        //public DataTable GetPaxDetailsForHoldPNR(string ORDERID)
        //{
        //    DataTable dtpax = new DataTable();
        //    try
        //    {
        //        con.Open();
        //        cmd = new SqlCommand("SP_DIST_GET_PAXINFOFORHOLDPNR", con);
        //        cmd.CommandType = CommandType.StoredProcedure;
        //        cmd.Parameters.AddWithValue("@ORDERID ", ORDERID);
        //        SqlDataReader reader = cmd.ExecuteReader();
        //        dtpax.Load(reader);

        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //    finally
        //    {
        //        cmd.Dispose();
        //        con.Close();
        //    }
        //    return dtpax;
        //}

        //public int UpdateLadgerByPaxId(string ORDERID, int PAXID, string TICKETNO)
        //{
        //    try
        //    {
        //        con.Open();
        //        SqlCommand cmd = new SqlCommand("SP_DIST_UPDATE_LEDGERBYPAXID", con);
        //        cmd.CommandType = CommandType.StoredProcedure;
        //        cmd.Parameters.AddWithValue("@ORDERID", ORDERID);
        //        cmd.Parameters.AddWithValue("@PAXID", PAXID);
        //        cmd.Parameters.AddWithValue("@TICKETNO", TICKETNO);
        //        int temp = 0;
        //        temp = cmd.ExecuteNonQuery();
        //        con.Close();
        //        return temp;
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine(ex.Message);
        //    }
        //    return 0;
        //}

        //public DataTable GetHoldPNRReport(string LOGINID, string OWNERID, string DISTRID, string USERTYPE, string STATUS, string MULTISTATUS)
        //{
        //    DataTable dthold = new DataTable();
        //    try
        //    {
        //        con.Open();
        //        cmd = new SqlCommand("SP_DIST_GET_HOLDPNRREPORT", con);
        //        cmd.CommandType = CommandType.StoredProcedure;
        //        cmd.Parameters.AddWithValue("@LOGINID ", LOGINID);
        //        cmd.Parameters.AddWithValue("@OWNERID ", OWNERID);
        //        cmd.Parameters.AddWithValue("@DISTRID ", DISTRID);
        //        cmd.Parameters.AddWithValue("@USERTYPE ", USERTYPE);
        //        cmd.Parameters.AddWithValue("@STATUS ", STATUS);
        //        cmd.Parameters.AddWithValue("@MULTISTATUS ", MULTISTATUS);
        //        SqlDataReader reader = cmd.ExecuteReader();
        //        dthold.Load(reader);

        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //    finally
        //    {
        //        cmd.Dispose();
        //        con.Close();
        //    }
        //    return dthold;
        //}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="jrnyType"></param>
        /// <param name="City"></param>
        /// <returns></returns>
        public List<FlightCityList> FetchCityName(string jrnyType, string City)
        {

            var listcity = new List<FlightCityList>();

            try
            {
                DbCommand DBCmd = new SqlCommand();
                DBCmd.CommandText = "SP_GET_CITYNAME";
                DBCmd.CommandType = CommandType.StoredProcedure;
                //DBHelper.AddInParameter(DBCmd, "@Airline", DbType.String, "");
                DBHelper.AddInParameter(DBCmd, "@Trip", DbType.String, jrnyType);
                DBHelper.AddInParameter(DBCmd, "@Code", DbType.String, City);
                IDataReader idr = DBHelper.ExecuteReader(DBCmd);
                while (idr.Read())
                {
                    FlightCityList FCCity = new FlightCityList();
                    FCCity.City = idr.GetString(idr.GetOrdinal("City"));
                    FCCity.AirportCode = idr.GetString(idr.GetOrdinal("AirportCode"));
                    FCCity.AirportName = idr.GetString(idr.GetOrdinal("AirportName"));
                    FCCity.CountryCode = idr.GetString(idr.GetOrdinal("CountryCode"));
                    listcity.Add(FCCity);
                }
            }
            catch (Exception ex)
            {
                //throw ex;
            }
            finally
            {
                //reader.Close();
                // cmd.Dispose();
            }
            return listcity;

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="AirlineCode"></param>
        /// <returns></returns>
        public List<AirlineList> FetchAirlineName(string AirlineCode)
        {

            var listairline = new List<AirlineList>();
            IDataReader reader;
            try
            {
                DbCommand DBCmd = new SqlCommand();
                DBCmd.CommandText = "SP_GET_AIRLINENAME";
                DBCmd.CommandType = CommandType.StoredProcedure;
                DBHelper.AddInParameter(DBCmd, "@CODE", DbType.String, AirlineCode);
                reader = DBHelper.ExecuteReader(DBCmd);
                while (reader.Read())
                {
                    AirlineList FC = new AirlineList();
                    FC.AlilineName = reader.GetString(reader.GetOrdinal("AirlineName"));
                    FC.AirlineCode = reader.GetString(reader.GetOrdinal("AirlineCode"));
                    listairline.Add(FC);
                }
            }
            catch (Exception ex)
            {
                //throw ex;
            }
            finally
            {
                //reader.Close();
                // cmd.Dispose();
            }
            return listairline;
        }
        /// <summary>
        /// MarkUp details
        /// </summary>
        /// <param name="AgentId"></param>
        /// <param name="DistrId"></param>
        /// <param name="UserType"></param>
        /// <param name="jrnyType"></param>
        /// <returns></returns>
        public DataTable GetMarkupDetails_Gal(string AgentId, string DistrId, string jrnyType, string UserType)
        {
            DbCommand cmd = new SqlCommand();
            cmd.CommandText = "GetMarkup";
            cmd.CommandType = CommandType.StoredProcedure;
            DBHelper.AddInParameter(cmd, "@agid", DbType.String, AgentId);
            DBHelper.AddInParameter(cmd, "@distrid", DbType.String, DistrId);
            DBHelper.AddInParameter(cmd, "@trip", DbType.String, jrnyType);
            DBHelper.AddInParameter(cmd, "@idtype", DbType.String, UserType);

            //DataSet MrkDs = new DataSet();
            DataTable MrkDt = new DataTable();
            MrkDt.Load(DBHelper.ExecuteReader(cmd));

            return MrkDt;
        }

        /// <summary>
        /// TDS For Galelio
        /// </summary>
        /// <param name="AgentId"></param>
        /// <param name="DistrId"></param>
        /// <param name="UserType"></param>
        /// <param name="jrnyType"></param>
        /// <returns></returns>
        public string GetTDSPercent_Gal(string UserId)
        {
            DbCommand cmd = new SqlCommand();
            cmd.CommandText = "GetTdsPrcnt";
            cmd.CommandType = CommandType.StoredProcedure;
            DBHelper.AddInParameter(cmd, "@UserId", DbType.String, UserId);
            string TDS = "0";
            try
            {
                TDS = Convert.ToString(DBHelper.ExecuteScalar(cmd));
            }
            catch(Exception ex)
            {
                TDS = "5";
                ITZERRORLOG.ExecptionLogger.FileHandling("FlightCommonDAL(FlightCommonDAL_GetTDSPercent_Gal)", "Error_001", ex, "GetTDSPercent_Gal");
            }            
            return TDS;
        }

        /// <summary>
        /// Comm For Galelio
        /// </summary>
        /// <param name="AgentId"></param>
        /// <param name="DistrId"></param>
        /// <param name="UserType"></param>
        /// <param name="jrnyType"></param>
        /// <returns></returns>
        public DataTable Cal_Comm_cb_Gal(string GPType, string VC, decimal BaseFare, decimal YQ, int PaxCnt, string RBD, string cabinClass, string depdate,string sector
                                         , string returnDate, string farebasis, string orgCountry, string destCountry, string fltNum,string OPC,string MktC,string fareType,string bkgChannel)
        {
            DataTable dt = new DataTable();
            DbCommand cmd = new SqlCommand();
            cmd.CommandText = "CalcComm";
            cmd.CommandType = CommandType.StoredProcedure;
            DBHelper.AddInParameter(cmd, "@GPType", DbType.String, GPType);
            DBHelper.AddInParameter(cmd, "@AirCode", DbType.String, VC);
            DBHelper.AddInParameter(cmd, "@BaseFare", DbType.Decimal, BaseFare);
            DBHelper.AddInParameter(cmd, "@YQ", DbType.Decimal, YQ);
            DBHelper.AddInParameter(cmd, "@PaxCnt", DbType.Int32, PaxCnt);
            DBHelper.AddInParameter(cmd, "@RBD", DbType.String, RBD);
            DBHelper.AddInParameter(cmd, "@cabinClass", DbType.String, cabinClass);
            DBHelper.AddInParameter(cmd, "@Depdate", DbType.String, depdate);
            DBHelper.AddInParameter(cmd, "@Sector", DbType.String, sector);

            DBHelper.AddInParameter(cmd, "@ReturnDate", DbType.String, returnDate);
            DBHelper.AddInParameter(cmd, "@Farebasis", DbType.String, farebasis);
            DBHelper.AddInParameter(cmd, "@OrgCountry", DbType.String, orgCountry);
            DBHelper.AddInParameter(cmd, "@DestCountry", DbType.String, destCountry);
            DBHelper.AddInParameter(cmd, "@FltNum", DbType.String, fltNum);
            DBHelper.AddInParameter(cmd, "@OPC", DbType.String, OPC);
            DBHelper.AddInParameter(cmd, "@MktC", DbType.String, MktC);
            DBHelper.AddInParameter(cmd, "@FareType", DbType.String, fareType);
            DBHelper.AddInParameter(cmd, "@BkgChannel", DbType.String, bkgChannel);

            dt.Load(DBHelper.ExecuteReader(cmd));
            return dt;
        }


        /// <summary>
        /// Commission for International
        /// </summary>
        /// <param name="GPType">Agency Type</param>
        /// <param name="VC">validating Carrier</param>
        /// <param name="BaseFare"></param>
        /// <param name="YQ"></param>
        /// <param name="Qtax"></param>
        /// <param name="Org">Origin</param>
        /// <param name="Dest">Destination</param>
        /// <param name="Cls">RBD</param>
        /// <param name="DDate">DDMMYY</param>
        /// <param name="RDate">DDMMYY</param>
        /// <param name="PaxCnt">1</param>
        /// <returns></returns>
        public DataTable Cal_Comm_cb_Gal_Intl(string GPType, string VC, decimal BaseFare, decimal YQ, decimal Qtax, string Org, string Dest, string Cls, string DDate, string RDate, string cabinClass, string farebasis, string fltNum, string OPC, string MktC, string fareType, string bkgChannel)
        {
            DataTable dt = new DataTable();
            try { 
            DbCommand cmd = new SqlCommand();
            cmd.CommandText = "CalcCommPlb";
            cmd.CommandType = CommandType.StoredProcedure;
            DBHelper.AddInParameter(cmd, "@GPType", DbType.String, GPType);
            DBHelper.AddInParameter(cmd, "@AirCode", DbType.String, VC);
            DBHelper.AddInParameter(cmd, "@BaseFare", DbType.Decimal, BaseFare);
            DBHelper.AddInParameter(cmd, "@YQ", DbType.Decimal, YQ);
            DBHelper.AddInParameter(cmd, "@Org", DbType.String, Org);
            DBHelper.AddInParameter(cmd, "@Dst", DbType.String, Dest);
            DBHelper.AddInParameter(cmd, "@Cls", DbType.String, Cls);
            DBHelper.AddInParameter(cmd, "@Qtax", DbType.Decimal, Qtax);
            DBHelper.AddInParameter(cmd, "@ddate", DbType.String, DDate);
            DBHelper.AddInParameter(cmd, "@rdate", DbType.String, RDate);
            DBHelper.AddInParameter(cmd, "@CabinClass", DbType.String, cabinClass);
            DBHelper.AddInParameter(cmd, "@Farebasis", DbType.String, farebasis);
            DBHelper.AddInParameter(cmd, "@FltNum", DbType.String, fltNum);
            DBHelper.AddInParameter(cmd, "@OPC", DbType.String, OPC);
            DBHelper.AddInParameter(cmd, "@MktC", DbType.String, MktC);
            DBHelper.AddInParameter(cmd, "@FareType", DbType.String, fareType);
            DBHelper.AddInParameter(cmd, "@BkgChannel", DbType.String, bkgChannel);
            dt.Load(DBHelper.ExecuteReader(cmd));
            }
            catch (Exception ex)
            {
                cmd.Dispose();
                ITZERRORLOG.ExecptionLogger.FileHandling("FlightCommonDAL(FlightCommonDAL_Cal_Comm_cb_Gal_Intl)", "Error_001", ex, "Cal_Comm_cb_Gal_Intl");
            }
            return dt;
        }


        /// <summary>
        /// Blcok Services details
        /// </summary>
        /// <param name="AgentId">userid/ALL</param>
        /// <param name="jrnyType">D/I</param>
        /// <returns></returns>
        public string GetBlockSrvcFromBD(string AgentId, string jrnyType)
        {
            string SrvList = "0";
            try
            {
                DbCommand cmd = new SqlCommand();
                cmd.CommandText = "sp_avilable_service_agent";
                cmd.CommandType = CommandType.StoredProcedure;
                DBHelper.AddInParameter(cmd, "@agent_id", DbType.String, AgentId);
                DBHelper.AddInParameter(cmd, "@trip", DbType.String, jrnyType);
                SrvList = Convert.ToString(DBHelper.ExecuteScalar(cmd));
            }
            catch(Exception ex)
            {
                ITZERRORLOG.ExecptionLogger.FileHandling("FlightCommonDAL(FlightCommonDAL_GetBlockSrvcFromBD)", "Error_001", ex, "GetBlockSrvcFromBD");
            }

            return SrvList;
        }

        public DataTable clac_MgtFeeFromDB(string GPType, string VC, decimal BaseFare, decimal YQ, string Trip, decimal Total)
        {
            DataTable dt = new DataTable();
            DbCommand cmd = new SqlCommand();
            cmd.CommandText = "SP_CORP_CALC_MANAGEMENTFEE";
            cmd.CommandType = CommandType.StoredProcedure;
            DBHelper.AddInParameter(cmd, "@TYPE", DbType.String, GPType);
            DBHelper.AddInParameter(cmd, "@AIRLINE", DbType.String, VC);
            DBHelper.AddInParameter(cmd, "@BASIC", DbType.Decimal, BaseFare);
            DBHelper.AddInParameter(cmd, "@YQ", DbType.Decimal, YQ);
            DBHelper.AddInParameter(cmd, "@TRIP", DbType.String, Trip);
            DBHelper.AddInParameter(cmd, "@TOTAL", DbType.Decimal, Total);
            dt.Load(DBHelper.ExecuteReader(cmd));
            return dt;
        }

        public string GetDealCodeFromDB(string Orderid)
        {
            string strCode = "";
            try
            {
                DbCommand cmd = new SqlCommand();
                cmd.CommandText = "checkfordealcode";
                cmd.CommandType = CommandType.StoredProcedure;
                DBHelper.AddInParameter(cmd, "@GdsPnr", DbType.String, Orderid);
                strCode = Convert.ToString(DBHelper.ExecuteScalar(cmd));
            }
            catch
            { }
            return strCode;
        }

        public float GetMiscServiceChargeFromDB(string Trip, string VC, string Agentid, string type, string org, string dest)
        {
            float Srvcharge = 0;
            try
            {
                DbCommand cmd = new SqlCommand();
                cmd.CommandText = "SP_GET_MISC_SRV_CHARGE";
                cmd.CommandType = CommandType.StoredProcedure;
                DBHelper.AddInParameter(cmd, "@Trip", DbType.String, Trip);
                DBHelper.AddInParameter(cmd, "@Code", DbType.String, VC);
                DBHelper.AddInParameter(cmd, "@agentid", DbType.String, Agentid);
                DBHelper.AddInParameter(cmd, "@type", DbType.String, type);
                DBHelper.AddInParameter(cmd, "@org", DbType.String, org);
                DBHelper.AddInParameter(cmd, "@dest", DbType.String, dest);
                Srvcharge = float.Parse(Convert.ToString(DBHelper.ExecuteScalar(cmd)));
            }
            catch
            { }
            return Srvcharge;
        }

        public string GetTicketingInfoFromDB(string Orderid)
        {
            string TicketingInfo = "";
            try
            {
                DbCommand cmd = new SqlCommand();
                cmd.CommandText = "SP_GetTicketingInfo";
                cmd.CommandType = CommandType.StoredProcedure;
                DBHelper.AddInParameter(cmd, "@Orderid", DbType.String, Orderid);
                TicketingInfo = Convert.ToString(DBHelper.ExecuteScalar(cmd));
            }
            catch
            { }
            return TicketingInfo;
        }

        public DataTable GetFQCSDeatailsFromDB()
        {
            DataTable dt = new DataTable();
            try
            {
                DbCommand cmd = new SqlCommand();
                cmd.CommandText = "SP_GetFQCSData";
                cmd.CommandType = CommandType.StoredProcedure;
                dt.Load(DBHelper.ExecuteReader(cmd));
            }
            catch
            { }
            return dt;
        }

        public List<BaggageList> GetBaggage(string Trip, string AirlineCode, bool Bag)
        {

            List<BaggageList> listbaggage = new List<BaggageList>();
            IDataReader reader;
            try
            {
                DbCommand DBCmd = new SqlCommand();
                DBCmd.CommandText = "SP_GETBAGGAGE_INFO";
                DBCmd.CommandType = CommandType.StoredProcedure;
                DBHelper.AddInParameter(DBCmd, "@TRIP", DbType.String, Trip);
                DBHelper.AddInParameter(DBCmd, "@AIRLINE", DbType.String, AirlineCode);
                DBHelper.AddInParameter(DBCmd, "@IsBagFare", DbType.Boolean, Bag);
                reader = DBHelper.ExecuteReader(DBCmd);
                while (reader.Read())
                {
                    BaggageList FC = new BaggageList();
                    FC.BaggageName = reader.GetString(reader.GetOrdinal("BaggageName"));
                    FC.Weight = reader.GetString(reader.GetOrdinal("Weight"));
                    FC.Trip = reader.GetString(reader.GetOrdinal("Trip"));
                    FC.Airline = reader.GetString(reader.GetOrdinal("Airline"));
                    FC.Class = reader.GetString(reader.GetOrdinal("Class"));
                    listbaggage.Add(FC);
                }
            }
            catch (Exception ex)
            {
                ITZERRORLOG.ExecptionLogger.FileHandling("FlightCommonDAL(FlightCommonDAL_GetBaggage)", "Error_001", ex, "GetBaggage");
                //throw ex;
            }
            finally
            {
                //reader.Close();
                // cmd.Dispose();
            }
            return listbaggage;
        }

        public List<BaggageList> GetBaggages(string Trip, string AirlineCode, bool Bag, string CountryFromCode, string CountryToCode)
        {

            List<BaggageList> listbaggage = new List<BaggageList>();
            IDataReader reader;
            try
            {
                DbCommand DBCmd = new SqlCommand();
                DBCmd.CommandText = "SP_GETBAGGAGE_INFONW";
                DBCmd.CommandType = CommandType.StoredProcedure;
                DBHelper.AddInParameter(DBCmd, "@TRIP", DbType.String, Trip);
                DBHelper.AddInParameter(DBCmd, "@AIRLINE", DbType.String, AirlineCode);
                DBHelper.AddInParameter(DBCmd, "@CountryFromCode", DbType.String, CountryFromCode);
                DBHelper.AddInParameter(DBCmd, "@CountryToCode", DbType.String, CountryToCode);
                DBHelper.AddInParameter(DBCmd, "@IsBagFare", DbType.Boolean, Bag);
                reader = DBHelper.ExecuteReader(DBCmd);
                while (reader.Read())
                {
                    BaggageList FC = new BaggageList();
                    FC.BaggageName = reader.GetString(reader.GetOrdinal("BaggageName"));
                    FC.Weight = reader.GetString(reader.GetOrdinal("Weight"));
                    FC.Trip = reader.GetString(reader.GetOrdinal("Trip"));
                    FC.Airline = reader.GetString(reader.GetOrdinal("Airline"));
                    FC.Class = reader.GetString(reader.GetOrdinal("Class"));
                    listbaggage.Add(FC);
                }
            }
            catch (Exception ex)
            {
                ITZERRORLOG.ExecptionLogger.FileHandling("FlightCommonDAL(FlightCommonDAL_GetBaggages)", "Error_001", ex, "GetBaggage");
                //throw ex;
            }
            finally
            {
                //reader.Close();
                // cmd.Dispose();
            }
            return listbaggage;
        }

        public DataTable GetofflineResultDal(string Org, string Dest, string depDate, string vc, string Trip)
        {

            DataTable dt = new DataTable();
            try
            {
                DbCommand DBCmd = new SqlCommand();
                DBCmd.CommandText = "SP_SearchCharteredFlight";
                DBCmd.CommandType = CommandType.StoredProcedure;
                DBHelper.AddInParameter(DBCmd, "@OrgDestFrom", DbType.String, Org);
                DBHelper.AddInParameter(DBCmd, "@OrgDestTo", DbType.String, Dest);
                DBHelper.AddInParameter(DBCmd, "@DepartureDate", DbType.String, depDate);
                DBHelper.AddInParameter(DBCmd, "@MarketingCarrier", DbType.String, vc);
                DBHelper.AddInParameter(DBCmd, "@Trip", DbType.String, Trip);
                dt.Load(DBHelper.ExecuteReader(DBCmd));

            }
            catch (Exception ex)
            {
                ITZERRORLOG.ExecptionLogger.FileHandling("FlightCommonDAL(FlightCommonDAL_GetofflineResultDal)", "Error_001", ex, "GetofflineResultDal");
                //throw ex;
            }
            finally
            {
                //reader.Close();
                // cmd.Dispose();
            }
            return dt;
        }

        public FlightSearch GetFltSearchDetailsDal_API(FlightSearch objSearch)
        {
            DataTable dt = new DataTable();
            
            try
            {
                DbCommand DBCmd = new SqlCommand();
                DBCmd.CommandText = "getSearchDetails_API";
                DBCmd.CommandType = CommandType.StoredProcedure;
                DBHelper.AddInParameter(DBCmd, "@Agentid", DbType.String, objSearch.UID);
                DBHelper.AddInParameter(DBCmd, "@Org", DbType.String, objSearch.HidTxtDepCity);
                DBHelper.AddInParameter(DBCmd, "@Dst", DbType.String, objSearch.HidTxtArrCity);
                DBHelper.AddInParameter(DBCmd, "@Airline", DbType.String, objSearch.HidTxtAirLine);
                dt.Load(DBHelper.ExecuteReader(DBCmd));
                if (dt.Rows.Count > 0)
                {
                    
                    objSearch.DepartureCity = dt.Rows[0]["Org"].ToString();
                    objSearch.ArrivalCity = dt.Rows[0]["Dst"].ToString();                                                          
                    objSearch.UserType = dt.Rows[0]["UserType"].ToString();
                    objSearch.DISTRID = dt.Rows[0]["DistrId"].ToString();
                    objSearch.TypeId = dt.Rows[0]["TypeId"].ToString();
                    objSearch.IsCorp = bool.Parse(dt.Rows[0]["IsCorp"].ToString());
                    objSearch.RTF = false;
                    objSearch.GDSRTF = false;
                    objSearch.AirLine = dt.Rows[0]["AirLine"].ToString();
                    objSearch.HidTxtAirLine = dt.Rows[0]["AirLine"].ToString();
                    objSearch.HidTxtDepCity = dt.Rows[0]["Orgcntry"].ToString();
                    objSearch.HidTxtArrCity = dt.Rows[0]["Dstcntry"].ToString();
                    objSearch.AgentType = dt.Rows[0]["AgentType"].ToString();                    
                }
            }
            catch(Exception ex)
            {
                ITZERRORLOG.ExecptionLogger.FileHandling("FlightCommonDAL(FlightCommonDAL_GetFltSearchDetailsDal_API)", "Error_001", ex, "GetFltSearchDetailsDal_API");
                throw ex;
            }
            return objSearch;
        }

        public DataTable GetFOPFromDB(string VC, string Trip, string Provider)
        {
            DataTable dt = new DataTable();
            try
            {
                DbCommand DBCmd = new SqlCommand();
                DBCmd.CommandText = "usp_flt_fop";
                DBCmd.CommandType = CommandType.StoredProcedure;
                DBHelper.AddInParameter(DBCmd, "@Trip", DbType.String, Trip);
                DBHelper.AddInParameter(DBCmd, "@Airline", DbType.String, VC);
                DBHelper.AddInParameter(DBCmd, "@Provider", DbType.String, Provider);
                dt.Load(DBHelper.ExecuteReader(DBCmd));

            }
            catch (Exception ex)
            {
                ITZERRORLOG.ExecptionLogger.FileHandling("FlightCommonDAL(FlightCommonDAL_GetFOPFromDB)", "Error_001", ex, "GetFOPFromDB");
                //throw ex;
            }
            finally
            {
                //reader.Close();
                // cmd.Dispose();
            }
            return dt;
        }
        //Cache update
        public List<MISCCharges> GetMiscCharges(string Trip, string VC, string Agentid, string type, string org, string dest)
        {

            List<MISCCharges> listmisc = new List<MISCCharges>();
            IDataReader reader;
            try
            {
                DbCommand cmd = new SqlCommand();
                //cmd.CommandText = "SP_GET_MISC_SRV_CHARGE_NEW";
                cmd.CommandText = "SP_GET_MISC_SRV_CHARGE_V1";                
                cmd.CommandType = CommandType.StoredProcedure;
                DBHelper.AddInParameter(cmd, "@Trip", DbType.String, Trip);
                DBHelper.AddInParameter(cmd, "@Code", DbType.String, VC);
                DBHelper.AddInParameter(cmd, "@agentid", DbType.String, Agentid);
                DBHelper.AddInParameter(cmd, "@type", DbType.String, type);
                DBHelper.AddInParameter(cmd, "@org", DbType.String, org);
                DBHelper.AddInParameter(cmd, "@dest", DbType.String, dest);
                reader = DBHelper.ExecuteReader(cmd);
                while (reader.Read())
                {
                    listmisc.Add(new MISCCharges
                    {
                        Airline = reader["AirlineCode"].ToString(),
                        Amount = float.Parse(reader["Amount"].ToString()),
                        
                        CommisionOnBasic = float.Parse(reader["CommisionOnBasic"].ToString()),
                        CommissionOnYq = float.Parse(reader["CommissionOnYq"].ToString()),
                        CommisionOnBasicYq = float.Parse(reader["CommisionOnBasicYq"].ToString()),
                        MarkupType = reader["MarkupType"].ToString(),
                        FareType = reader["FareType"].ToString()
                    });
                    //MISCCharges FC = new MISCCharges();
                    //FC.Airline = reader.GetString(reader.GetOrdinal("AirlineCode"));
                    //FC.Amount  = reader.GetFloat(reader.GetOrdinal("Amount"));
                    //listmisc.Add(FC);
                }
            }
            catch (Exception ex)
            {
                ITZERRORLOG.ExecptionLogger.FileHandling("FlightCommonDAL(FlightCommonDAL_GetMiscCharges)", "Error_001", ex, "GetMiscCharges");
                throw ex;
            }
            finally
            {
                //reader.Close();
                // cmd.Dispose();
            }
            return listmisc;
        }
        public int InsertCache(string Sector, string Searchdate, string Url, string Airline, string Response)
        {
            int InsCnt = 0;
            try
            {
                DbCommand cmd = new SqlCommand();
                cmd.CommandText = "usp_insert_cache_data";
                cmd.CommandType = CommandType.StoredProcedure;
                DBHelper.AddInParameter(cmd, "@sector", DbType.String, Sector);
                DBHelper.AddInParameter(cmd, "@searchdate", DbType.String, Searchdate );
                DBHelper.AddInParameter(cmd, "@url", DbType.String, Url);
                DBHelper.AddInParameter(cmd, "@Airline", DbType.String, Airline );
                DBHelper.AddInParameter(cmd, "@jsonResult", DbType.String, Response);
                InsCnt = DBHelper.ExecuteNonQuery(cmd);
            }
            catch(Exception ex)
            {
                ITZERRORLOG.ExecptionLogger.FileHandling("FlightCommonDAL(FlightCommonDAL_InsertCache)", "Error_001", ex, "InsertCache");
            }
            return InsCnt;
        }
        public int waitInsert(FlightSearch obj, int sec, bool iscpn, string aircode)
        {
            int InsCnt = 0;
            try
            {
                DbCommand cmd = new SqlCommand();
                cmd.CommandText = "Sp_InsertWaiting";
                cmd.CommandType = CommandType.StoredProcedure;
                DBHelper.AddInParameter(cmd, "@AgentID", DbType.String, obj.UID);
                DBHelper.AddInParameter(cmd, "@sector", DbType.String, obj.HidTxtDepCity.Split(',')[0] + ":" + obj.HidTxtArrCity.Split(',')[0]);
                DBHelper.AddInParameter(cmd, "@searchdate", DbType.String, obj.DepDate);
                DBHelper.AddInParameter(cmd, "@Airline", DbType.String, obj.HidTxtAirLine == "" ? aircode : obj.HidTxtAirLine);
                DBHelper.AddInParameter(cmd, "@SearchID", DbType.String, (obj.IsCorp == true ? "IsCorp:True" : "IsCorp:False") + "," + (iscpn == true ? "coupon:True" : "coupon:False"));
                DBHelper.AddInParameter(cmd, "@waitInSec", DbType.String, Convert.ToString(sec));
                InsCnt = DBHelper.ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                  ExecptionLogger.FileHandling("FlightCommonDAL(waitInsert)", "Error_001", ex, "waitInsert");
            }
            return InsCnt;
        }
        public DataSet GetSequenceTable(string Sector, string Searchdate, string Airline, string RetDate, bool IsRoundTrip, int pax)
        {

            DataSet ds = new DataSet();
            try
            {
                DbCommand DBCmd = new SqlCommand();
                DBCmd.CommandText = "usp_flt_get_Sequence_data";
                DBCmd.CommandType = CommandType.StoredProcedure;
                DBHelper.AddInParameter(DBCmd, "@sector", DbType.String, Sector);
                DBHelper.AddInParameter(DBCmd, "@searchdate", DbType.String, Searchdate);
                DBHelper.AddInParameter(DBCmd, "@Airline", DbType.String, Airline);
                DBHelper.AddInParameter(DBCmd, "@returndate", DbType.String, RetDate);
                DBHelper.AddInParameter(DBCmd, "@Isroundtrip", DbType.Boolean, IsRoundTrip);
                DBHelper.AddInParameter(DBCmd, "@paxs", DbType.Int32, pax);
                //ds.Load(DBHelper.ExecuteDataSet(DBCmd));
                ds = DBHelper.ExecuteDataSet(DBCmd);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                //reader.Close();
                // cmd.Dispose();
            }
            return ds;
        }
        //public DataSet GetSequenceTable(string Sector, string Searchdate, string Airline, string RetDate, bool IsRoundTrip)
        //{

        //    DataSet ds = new DataSet();
        //    try
        //    {
        //        DbCommand DBCmd = new SqlCommand();
        //        DBCmd.CommandText = "usp_flt_get_Sequence_data";
        //        DBCmd.CommandType = CommandType.StoredProcedure;
        //        DBHelper.AddInParameter(DBCmd, "@sector", DbType.String, Sector);
        //        DBHelper.AddInParameter(DBCmd, "@searchdate", DbType.String, Searchdate);
        //        DBHelper.AddInParameter(DBCmd, "@Airline", DbType.String, Airline);
        //        DBHelper.AddInParameter(DBCmd, "@returndate", DbType.String, RetDate);
        //        DBHelper.AddInParameter(DBCmd, "@Isroundtrip", DbType.Boolean, IsRoundTrip);
        //        //ds.Load(DBHelper.ExecuteDataSet(DBCmd));
        //        ds = DBHelper.ExecuteDataSet(DBCmd);

        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //        //reader.Close();
        //        // cmd.Dispose();
        //    }
        //    return ds;
        //}
        public DataSet GetSequenceTablebydate(string Sector, string Searchdate, string Airline, string RetDate, bool IsRoundTrip, string triptype, string trip, int pax)
        {

            DataSet ds = new DataSet();
            try
            {
                DbCommand DBCmd = new SqlCommand();
                DBCmd.CommandText = "usp_flt_get_Sequence_dataByDate";
                DBCmd.CommandType = CommandType.StoredProcedure;
                DBHelper.AddInParameter(DBCmd, "@sector", DbType.String, Sector);
                DBHelper.AddInParameter(DBCmd, "@searchdate", DbType.String, Searchdate);
                DBHelper.AddInParameter(DBCmd, "@Airline", DbType.String, Airline);
                DBHelper.AddInParameter(DBCmd, "@returndate", DbType.String, RetDate);
                DBHelper.AddInParameter(DBCmd, "@Isroundtrip", DbType.Boolean, IsRoundTrip);
                DBHelper.AddInParameter(DBCmd, "@triptype", DbType.String, triptype);
                DBHelper.AddInParameter(DBCmd, "@trip", DbType.String, trip);
                DBHelper.AddInParameter(DBCmd, "@paxs", DbType.Int32, pax);
                //ds.Load(DBHelper.ExecuteDataSet(DBCmd));
                ds = DBHelper.ExecuteDataSet(DBCmd);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                //reader.Close();
                // cmd.Dispose();
            }
            return ds;
        }

        public DataSet GetCacheTable(string Sector, string Searchdate, string Url, string UrlR, string Airline,string RetDate,bool IsRoundTrip)
        {

            DataSet ds = new DataSet();
            try
            {
                DbCommand DBCmd = new SqlCommand();
                DBCmd.CommandText = "usp_flt_get_cache_data";
                DBCmd.CommandType = CommandType.StoredProcedure;
                DBHelper.AddInParameter(DBCmd, "@sector", DbType.String, Sector );
                DBHelper.AddInParameter(DBCmd, "@searchdate", DbType.String, Searchdate );
                DBHelper.AddInParameter(DBCmd, "@url", DbType.String, Url );
                DBHelper.AddInParameter(DBCmd, "@urlR", DbType.String, UrlR );
                DBHelper.AddInParameter(DBCmd, "@Airline", DbType.String, Airline);
                DBHelper.AddInParameter(DBCmd, "@returndate", DbType.String, RetDate );
                DBHelper.AddInParameter(DBCmd, "@Isroundtrip", DbType.Boolean, IsRoundTrip);
                //ds.Load(DBHelper.ExecuteDataSet(DBCmd));
                ds=  DBHelper.ExecuteDataSet(DBCmd);

            }
            catch (Exception ex)
            {
                ITZERRORLOG.ExecptionLogger.FileHandling("FlightCommonDAL(FlightCommonDAL_GetCacheTable)", "Error_001", ex, "GetCacheTable");
                throw ex;
            }
            finally
            {
                //reader.Close();
                // cmd.Dispose();
            }
            return ds;
        }
        public string GetActiveAirlineProvider(string org, string dest, string airline, string rtf, string trip, string agentID,DateTime DepDate,DateTime RetDate)
        { //rtf: gds- gdsrountrip, lcc-lcc roundtrip, ''- for rest

            string providers = "";
            try
            {
                DbCommand cmd = new SqlCommand();
                cmd.CommandText = "usp_flt_Get_Active_Airline_For_Search_New"; // Use Live Enviroment
                //cmd.CommandText = "usp_flt_Get_Active_Airline_For_Search_New_Live";//// Use Live Crd Local Enviroment
                cmd.CommandType = CommandType.StoredProcedure;
                DBHelper.AddInParameter(cmd, "@Source", DbType.String, org);
                DBHelper.AddInParameter(cmd, "@Dest", DbType.String, dest);
                DBHelper.AddInParameter(cmd, "@Airline", DbType.String, airline);
                DBHelper.AddInParameter(cmd, "@RTF", DbType.String, rtf);
                DBHelper.AddInParameter(cmd, "@Trip", DbType.String, trip);
                DBHelper.AddInParameter(cmd, "@AgentID", DbType.String, agentID);
                DBHelper.AddInParameter(cmd, "@DepDate", DbType.DateTime, DepDate);
                DBHelper.AddInParameter(cmd, "@RetDate", DbType.DateTime, RetDate);
                providers = Convert.ToString(DBHelper.ExecuteScalar(cmd));

            }
            catch (Exception ex)
            {
                ITZERRORLOG.ExecptionLogger.FileHandling("FlightCommonDAL(FlightCommonDAL_GetActiveAirlineProvider)", "Error_001", ex, "GetActiveAirlineProvider");
            }
            finally
            {
                //reader.Close();
                // cmd.Dispose();
            }
            return providers;
        }
        public List<UserFlightSearch> GetUserFlightSearch(string UserId, string UserType)
        {

            List<UserFlightSearch> listfs = new List<UserFlightSearch>();
            IDataReader reader;
            try
            {
                DbCommand cmd = new SqlCommand();
                cmd.CommandText = "usp_get_details_fltsearch";
                cmd.CommandType = CommandType.StoredProcedure;
                DBHelper.AddInParameter(cmd, "@UserId", DbType.String, UserId);
                DBHelper.AddInParameter(cmd, "@UserType", DbType.String, UserType);

                reader = DBHelper.ExecuteReader(cmd);
                while (reader.Read())
                {
                    listfs.Add(new UserFlightSearch
                    {
                        UserId = reader["UserId"].ToString(),
                        TypeId = reader["TypeId"].ToString(),
                        UserType = reader["UserType"].ToString(),
                        TDS = reader["TDS"].ToString(),
                        AgentType = reader["AgentType"].ToString()
                    });
                    //MISCCharges FC = new MISCCharges();
                    //FC.Airline = reader.GetString(reader.GetOrdinal("AirlineCode"));
                    //FC.Amount  = reader.GetFloat(reader.GetOrdinal("Amount"));
                    //listmisc.Add(FC);
                }
            }
            catch (Exception ex)
            {
                ITZERRORLOG.ExecptionLogger.FileHandling("FlightCommonDAL(FlightCommonDAL_GetUserFlightSearch)", "Error_001", ex, "GetUserFlightSearch");
                //throw ex;
            }
            finally
            {
                //reader.Close();
                // cmd.Dispose();
            }
            return listfs;
        }



        public int InsertTBOBookingLog(string airline, string orderId, string pnr, string bookReq, string bookResp, string RePriceReq, string RePriceRes,string exep)
        {
            int SrvList = 0;
            try
            {
                DbCommand cmd = new SqlCommand();
                cmd.CommandText = "Usp_Insert_TBO_Booking_Logs";
                cmd.CommandType = CommandType.StoredProcedure;
                DBHelper.AddInParameter(cmd, "@Airline", DbType.String, airline);
                DBHelper.AddInParameter(cmd, "@OrderId", DbType.String, orderId);
                DBHelper.AddInParameter(cmd, "@PnrNo", DbType.String, pnr);
                DBHelper.AddInParameter(cmd, "@BookReq", DbType.String, bookReq);
                DBHelper.AddInParameter(cmd, "@BookRes", DbType.String, bookResp);
                DBHelper.AddInParameter(cmd, "@RePriceReq", DbType.String, RePriceReq);
                DBHelper.AddInParameter(cmd, "@RePriceRes", DbType.String, RePriceRes);
                DBHelper.AddInParameter(cmd, "@OTHERS", DbType.String, exep);
                SrvList = Convert.ToInt32(DBHelper.ExecuteNonQuery(cmd));


                
            }
            catch(Exception ex)
            {
                ITZERRORLOG.ExecptionLogger.FileHandling("FlightCommonDAL(FlightCommonDAL_InsertTBOBookingLog)", "Error_001", ex, "InsertTBOBookingLog");
            }

            return SrvList;
        }

        public int InsertYABookingLog(string airline, string orderId, string pnr, string bookReq, string bookResp, string RePriceReq, string RePriceRes, string exep)
        {
            int SrvList = 0;
            try
            {
                DbCommand cmd = new SqlCommand();
                cmd.CommandText = "Usp_Insert_YA_Booking_Logs";
                cmd.CommandType = CommandType.StoredProcedure;
                DBHelper.AddInParameter(cmd, "@Airline", DbType.String, airline);
                DBHelper.AddInParameter(cmd, "@OrderId", DbType.String, orderId);
                DBHelper.AddInParameter(cmd, "@PnrNo", DbType.String, pnr);
                DBHelper.AddInParameter(cmd, "@BookReq", DbType.String, bookReq);
                DBHelper.AddInParameter(cmd, "@BookRes", DbType.String, bookResp);
                DBHelper.AddInParameter(cmd, "@RePriceReq", DbType.String, RePriceReq);
                DBHelper.AddInParameter(cmd, "@RePriceRes", DbType.String, RePriceRes);
                DBHelper.AddInParameter(cmd, "@OTHERS", DbType.String, exep);
                SrvList = Convert.ToInt32(DBHelper.ExecuteNonQuery(cmd));

            }
            catch
            { }

            return SrvList;
        }



        public DataSet GetSearchCredentilas(string Provider)
        {

            DataSet ds = new DataSet();
            try
            {
                DbCommand DBCmd = new SqlCommand();
                DBCmd.CommandText = "GetSrvCredentials";
                DBCmd.CommandType = CommandType.StoredProcedure;
                DBHelper.AddInParameter(DBCmd, "@Provider", DbType.String, Provider);            
               
                ds = DBHelper.ExecuteDataSet(DBCmd);

            }
            catch (Exception ex)
            {
                ITZERRORLOG.ExecptionLogger.FileHandling("FlightCommonDAL(FlightCommonDAL_GetSearchCredentilas)", "Error_001", ex, "GetSearchCredentilas");
                throw ex;
            }
            finally
            {
                //reader.Close();
                // cmd.Dispose();
            }
            return ds;
        }



        public DataSet InserAndGetYAPricingLog(string orderId, string priceReq, string priceRes,string cmdType)
        {

            DataSet ds = new DataSet();
            try
            {
                DbCommand DBCmd = new SqlCommand();
                DBCmd.CommandText = "usp_InsertAndGetPricingLog";
                DBCmd.CommandType = CommandType.StoredProcedure;
                DBHelper.AddInParameter(DBCmd, "@orderId", DbType.String, orderId);
                DBHelper.AddInParameter(DBCmd, "@PriceReq", DbType.String, priceReq);
                DBHelper.AddInParameter(DBCmd, "@PriceRes", DbType.String, priceRes);
                DBHelper.AddInParameter(DBCmd, "@CmdType", DbType.String, cmdType);
                ds = DBHelper.ExecuteDataSet(DBCmd);

            }
            catch (Exception ex)
            {                
                ITZERRORLOG.ExecptionLogger.FileHandling("FlightCommonDAL(FlightCommonDAL_InserAndGetYAPricingLog)", "Error_001", ex, "InserAndGetYAPricingLog");
                throw ex;
            }
            finally
            {
                //reader.Close();
                // cmd.Dispose();
            }
            return ds;
        }




        public int AddFareRule(string orderId, string fareRule)
        {

            int status = 0;
            try
            {
                DbCommand DBCmd = new SqlCommand();
                DBCmd.CommandText = "usp_AddFareRule";
                DBCmd.CommandType = CommandType.StoredProcedure;
                DBHelper.AddInParameter(DBCmd, "@OrderID", DbType.String, orderId);
                DBHelper.AddInParameter(DBCmd, "@FareRule", DbType.String, fareRule);
               
                status = DBHelper.ExecuteNonQuery(DBCmd);               	

            }
            catch (Exception ex)
            {
                ITZERRORLOG.ExecptionLogger.FileHandling("FlightCommonDAL(FlightCommonDAL_AddFareRule)", "Error_001", ex, "AddFareRule");
                throw ex;
            }
            finally
            {
                //reader.Close();
                // cmd.Dispose();
            }
            return status;
        }



        public int InserScrapBookingLog(string orderId, string VC, string Pnr, string request,string response,string other)
        {
            int status = 0;
          
            try
            {
                DbCommand DBCmd = new SqlCommand();
                DBCmd.CommandText = "usp_insert_scrap_booking_Log";
                DBCmd.CommandType = CommandType.StoredProcedure;
                DBHelper.AddInParameter(DBCmd, "@OrderID", DbType.String, orderId);
                DBHelper.AddInParameter(DBCmd, "@PNR", DbType.String, Pnr);
                DBHelper.AddInParameter(DBCmd, "@VC", DbType.String, VC);
                DBHelper.AddInParameter(DBCmd, "@Request", DbType.String, request);
                DBHelper.AddInParameter(DBCmd, "@Response", DbType.String, response);
                DBHelper.AddInParameter(DBCmd, "@Other", DbType.String, other);
               
                DBHelper.ExecuteNonQuery(DBCmd);
                status = 1;

            }
            catch (Exception ex)
            {
                ITZERRORLOG.ExecptionLogger.FileHandling("FlightCommonDAL(FlightCommonDAL_InserScrapBookingLog)", "Error_001", ex, "InserScrapBookingLog");
                // throw ex;
            }
            finally
            {
                //reader.Close();
                // cmd.Dispose();
            }
            return status;
        }


        public List<FareTypeSettings> GetFareTypeSettings(string aircode, string trip, string provider)
        {

            DataTable dt = new DataTable();
            DbCommand cmd = new SqlCommand();
            List<FareTypeSettings> list = new List<FareTypeSettings>();
            try
            {
                cmd.CommandText = "usp_flt_GetFareTypeSettings";
                cmd.CommandType = CommandType.StoredProcedure;
                DBHelper.AddInParameter(cmd, "@AirCode", DbType.String, aircode);
                DBHelper.AddInParameter(cmd, "@Trip", DbType.String, trip);
                DBHelper.AddInParameter(cmd, "@Provider", DbType.String, provider);
                dt.Load(DBHelper.ExecuteReader(cmd));


                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    list.Add(new FareTypeSettings
                    {
                        AirCode = Convert.ToString(dt.Rows[i]["AirCode"]),
                        Trip = Convert.ToString(dt.Rows[i]["Trip"]),
                        FareClass_Req = Convert.ToString(dt.Rows[i]["FareClass_Req"]),
                        FareClass_Res = Convert.ToString(dt.Rows[i]["FareClass_Res"]),
                        FareType = Convert.ToString(dt.Rows[i]["FareType"]),
                        ProductClass_Req = Convert.ToString(dt.Rows[i]["ProductClass_Req"]),
                        ProductClass_Res = Convert.ToString(dt.Rows[i]["ProductClass_Res"]),
                        FSK = Convert.ToString(dt.Rows[i]["FSK"]),
                        CancellationPanelty = Convert.ToDecimal(dt.Rows[i]["CancellationPanelty"]),
                        ReIssuePanelty = Convert.ToDecimal(dt.Rows[i]["ReIssuePanelty"]),
                        IsRefundable = Convert.ToBoolean(dt.Rows[i]["IsRefundable"]),
                        Provider = Convert.ToString(dt.Rows[i]["Provider"]),
                        IdType = Convert.ToString(dt.Rows[i]["IdType"]),
                        IsChangeable = Convert.ToBoolean(dt.Rows[i]["IsChangeable"]),
                        FareBasis = Convert.ToString(dt.Rows[i]["FareBasis"]),
                        CreatedDate = Convert.ToString(dt.Rows[i]["CreatedDate"]),
                        Cabin = Convert.ToString(dt.Rows[i]["Cabin"]),
                        IsBagFare=Convert.ToBoolean(dt.Rows[i]["IsBagFare"]),
                        IsSMEFare = Convert.ToBoolean(dt.Rows[i]["IsSMEFare"])

                    });
                }
                cmd.Dispose();
            }
            catch (Exception ex) {
                cmd.Dispose();
                ITZERRORLOG.ExecptionLogger.FileHandling("FlightCommonDAL(FlightCommonDAL_GetFareTypeSettings)", "Error_001", ex, "GetFareTypeSettings");
            }//ExecptionLogger.FileHandling("DAL_FlightCommanDAL(GetFareTypeSettings)", "Error_007", ex, "Flight");
            return list;
        }
        #region GST
        public int UpdateGSTTax(string OrderId, double GSTDIFF)
        {
            int i = 0;
            try
            {


                SqlCommand cmd = new SqlCommand("USP_UPDATE_GST_TAXES");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@OrderId", OrderId);
                cmd.Parameters.AddWithValue("@GSTDIFF", GSTDIFF);
                i = DBHelper.ExecuteNonQuery(cmd);
                cmd.Dispose();

            }
            catch (Exception ex)
            {
                cmd.Dispose();
                ITZERRORLOG.ExecptionLogger.FileHandling("FlightCommonDAL(FlightCommonDAL_UpdateGSTTax)", "Error_001", ex, "UpdateGSTTax");
                // ExecptionLogger.FileHandling("DAL_FlightCommanDAL(UpdateGSTTax)", "Error_007", ex, "Flight");
            }
            return i;
        }
        #endregion


        public List<SearchResultBlock> GetFlightResultBlockList(string Trip, string Provider, string Airline, string GroupType, string AgentId, string FareType, string Exclude)
        {

            DataTable dt = new DataTable();
            DbCommand cmd = new SqlCommand();
            List<SearchResultBlock> list = new List<SearchResultBlock>();
            try
            {
                cmd.CommandText = "Sp_GetSearchResultBlock";
                cmd.CommandType = CommandType.StoredProcedure;
                DBHelper.AddInParameter(cmd, "@Trip", DbType.String, Trip);
                DBHelper.AddInParameter(cmd, "@Provider", DbType.String, Provider);
                DBHelper.AddInParameter(cmd, "@Airline", DbType.String, Airline);
                DBHelper.AddInParameter(cmd, "@GroupType", DbType.String, GroupType);
                DBHelper.AddInParameter(cmd, "@AgentId", DbType.String, AgentId);
                DBHelper.AddInParameter(cmd, "@FareType", DbType.String, FareType);
                DBHelper.AddInParameter(cmd, "@Exclude", DbType.String, Exclude);
                dt.Load(DBHelper.ExecuteReader(cmd));
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    list.Add(new SearchResultBlock
                    {                        
                        Provider = Convert.ToString(dt.Rows[i]["Provider"]),                        
                        Trip = Convert.ToString(dt.Rows[i]["Trip"]),
                        Airline = Convert.ToString(dt.Rows[i]["Airline"]),
                        AirlineType = Convert.ToString(dt.Rows[i]["AirlineType"]),
                        FareType = Convert.ToString(dt.Rows[i]["FareType"]),                        
                        GroupType = Convert.ToString(dt.Rows[i]["GroupType"]),
                        AgentId = Convert.ToString(dt.Rows[i]["AgentId"]),                        
                        RTF = Convert.ToBoolean(dt.Rows[i]["RTF"]),
                        Status = Convert.ToBoolean(dt.Rows[i]["Status"]),
                        AgentIdExclude = Convert.ToString(dt.Rows[i]["AgentIdExclude"]),
                        ExcludeStatus = Convert.ToString(dt.Rows[i]["Exclude"])

                    });
                }
                cmd.Dispose();
            }
            catch (Exception ex) {
                cmd.Dispose();
                ITZERRORLOG.ExecptionLogger.FileHandling("FlightCommonDAL(FlightCommonDAL_GetFlightResultBlockList)", "Error_001", ex, "GetFlightResultBlockList");
            }
            return list;
        }

        #region Get Domestic Commission Total Records 
        public DataTable GetDomCommissionRecord(string AgentId, string AgentDistrId, string AgentType, string VC, string depdate, string sector,string returnDate,string fareType, string bkgChannel)
        {
            DataTable dt = new DataTable();
            DbCommand cmd = new SqlCommand();
            try
            {
                cmd.CommandText = "SpGetDomCommissionRecord";
                cmd.CommandType = CommandType.StoredProcedure;
                DBHelper.AddInParameter(cmd, "@AgentId", DbType.String, AgentId);
                DBHelper.AddInParameter(cmd, "@AgentDistrId", DbType.String, AgentDistrId);                
                DBHelper.AddInParameter(cmd, "@AgentType", DbType.String, AgentType);
                DBHelper.AddInParameter(cmd, "@AirCode", DbType.String, VC);
                DBHelper.AddInParameter(cmd, "@Depdate", DbType.String, depdate);
                DBHelper.AddInParameter(cmd, "@Sector", DbType.String, sector);
                DBHelper.AddInParameter(cmd, "@ReturnDate", DbType.String, returnDate);
                DBHelper.AddInParameter(cmd, "@FareType", DbType.String, fareType);               
                dt.Load(DBHelper.ExecuteReader(cmd));
                cmd.Dispose();
            }
            catch(Exception ex)
            {
                cmd.Dispose();
                ITZERRORLOG.ExecptionLogger.FileHandling("FlightCommonDAL(FlightCommonDAL_GetFlightResultBlockList)", "Error_001", ex, "GetFlightResultBlockList");
            }
           
            return dt;
        }

        #endregion

        #region GetMailNotificationCredential for Api Service
        public DataSet GetMailNotificationCredential(string Department)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand DBCmd = new SqlCommand();
                DBCmd.CommandText = "SP_GetMailNotificationCredential";
                DBCmd.CommandType = CommandType.StoredProcedure;
                DBHelper.AddInParameter(DBCmd, "@Department", DbType.String, Department);
                //ds.Load(DBHelper.ExecuteDataSet(DBCmd));
                ds = DBHelper.ExecuteDataSet(DBCmd);
                DBCmd.Dispose();
            }
            catch (Exception ex)
            {
                ITZERRORLOG.ExecptionLogger.FileHandling("FlightCommonDAL(GetMailNotificationCredential)", "Error_001", ex, "GetMailNotificationCredential");
                // throw ex;
            }
            finally
            {
                //reader.Close();
                // cmd.Dispose();
            }
            return ds;
        }
        #endregion

        #region Create TextFile in Date wise in Drive
        public void WriteLogFileByDate(string TxtFileName,string ReferenceNo, string Module, string ServiceType, string MethodName, string Description, string MoreDescription)
        {            
            try
            {                
                string ErrorLogPath = ConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                // Specify a "currently active folder" 
                string activeDir = ErrorLogPath + DateTime.Now.Date.ToString("dd-MMM-yyyy");                
                // Creating the folder
                System.IO.DirectoryInfo objDirectoryInfo = new System.IO.DirectoryInfo(activeDir);
                if (!System.IO.Directory.Exists(objDirectoryInfo.FullName))
                {
                    string newPath = "", newFileName = "";
                    try
                    {
                        // Create a new file name. This example generates 
                        System.IO.Directory.CreateDirectory(activeDir);
                        newFileName = System.IO.Path.GetFileName(TxtFileName.ToString() + ".txt");
                        // Combine the new file name with the path
                        newPath = System.IO.Path.Combine(activeDir, newFileName);
                    }
                    catch (Exception ex)
                    {
                        //string activeDir2 = @"D:\ITZError_Folder_\" + DateTime.Now.Date.ToString("dd-MMM-yyyy");
                        string activeDir2 = ErrorLogPath + DateTime.Now.Date.ToString("dd-MMM-yyyy");
                        System.IO.DirectoryInfo objDirectoryInfo2 = new System.IO.DirectoryInfo(activeDir2);
                        // Create a new file name. This example generates
                        System.IO.Directory.CreateDirectory(activeDir2);
                        newFileName = System.IO.Path.GetFileName(TxtFileName.ToString() + ".txt");
                        // Combine the new file name with the path
                        newPath = System.IO.Path.Combine(activeDir2, newFileName);
                    }
                    //// Create a new file name. This example generates 
                    //string newFileName = Path.GetFileName(Err_Service.ToString() + ".txt");
                    //// Combine the new file name with the path
                    //string newPath = Path.Combine(activeDir, newFileName);
                    //string TxtFileName,string ReferenceNo, string Module, string ServiceType, string MethodName, string Description, string MoreDescription
                    System.IO.FileStream fs = new System.IO.FileStream(newPath, System.IO.FileMode.Append, System.IO.FileAccess.Write);
                    System.IO.StreamWriter sw = new System.IO.StreamWriter(fs);
                    sw.Write(sw.NewLine);
                    sw.WriteLine("Date-Time:" + DateTime.Now.ToString());
                    sw.Write(sw.NewLine);
                    sw.WriteLine("FileName" + TxtFileName);
                    sw.Write(sw.NewLine);
                    sw.WriteLine("ReferenceNo:" + ReferenceNo);
                    sw.Write(sw.NewLine);
                    sw.WriteLine("Module:" + Module);
                    sw.Write(sw.NewLine);
                    sw.WriteLine("ServiceType:" + ServiceType);
                    sw.Write(sw.NewLine);
                    sw.WriteLine("MethodName:" + MethodName);
                    sw.Write(sw.NewLine);
                    sw.WriteLine("Description:" + Description);
                    sw.Write(sw.NewLine);
                    sw.WriteLine("MoreDescription:" + MoreDescription);
                    sw.Write(sw.NewLine);
                    sw.Flush();
                    sw.Close();
                    fs.Close();
                }
                else
                {
                    string newFileName = System.IO.Path.GetFileName(TxtFileName.ToString() + ".txt");
                    // Combine the new file name with the path
                    string newPath = System.IO.Path.Combine(activeDir, newFileName);
                    System.IO.FileStream fs = new System.IO.FileStream(newPath, System.IO.FileMode.Append, System.IO.FileAccess.Write);
                    System.IO.StreamWriter sw = new System.IO.StreamWriter(fs);
                    sw.Write(sw.NewLine);
                    sw.WriteLine("Date-Time:" + DateTime.Now.ToString());
                    sw.Write(sw.NewLine);
                    sw.WriteLine("FileName" + TxtFileName);
                    sw.Write(sw.NewLine);
                    sw.WriteLine("ReferenceNo:" + ReferenceNo);
                    sw.Write(sw.NewLine);
                    sw.WriteLine("Module:" + Module);
                    sw.Write(sw.NewLine);
                    sw.WriteLine("ServiceType:" + ServiceType);
                    sw.Write(sw.NewLine);
                    sw.WriteLine("MethodName:" + MethodName);
                    sw.Write(sw.NewLine);
                    sw.WriteLine("Description:" + Description);
                    sw.Write(sw.NewLine);
                    sw.WriteLine("MoreDescription:" + MoreDescription);
                    sw.Write(sw.NewLine);
                    sw.Flush();
                    sw.Close();
                    fs.Close();
                    //}

                }
            }
            catch (Exception ex)
            {

            }
        }
        #endregion
		  #region Booking LCC
        public DataSet GetAllFlightDetailsByOrderId(string orderId)
        {

            DataSet Ds = new DataSet();
            try
            {

                SqlCommand sqlcmd = new SqlCommand();
                sqlcmd.CommandText = "usp_GetAllFlightDetailsByOrderID";
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.CommandTimeout = 60;
                sqlcmd.Parameters.AddWithValue("@OrderId", orderId);
                Ds = DBHelper.ExecuteDataSet(sqlcmd);
                sqlcmd.Dispose();
            }
            catch (Exception ex)
            {
                ExecptionLogger.FileHandling("DAL_FlightCommanDAL(GetAllFlightDetailsByOrderId)", "Error_007", ex, "Flight");
            }

            return Ds;
        }
        #endregion
        public string GetServiceBundleCode(string OrderId)
        {
            string status = "";
            try
            {
                SqlCommand cmd = new SqlCommand("USP_GET_ServiceBundleCode");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@OrderId", OrderId);
                status = DBHelper.ExecuteScalar(cmd).ToString();
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                status = "";
                ExecptionLogger.FileHandling("DAL_FlightCommanDAL(GetServiceBundleCode)", "Error_007", ex, "Flight");
            }

            return status;

        }
        public BookingResource getAddress(string TCCode)
        {
            BookingResource obj = new BookingResource();
            try
            {
                DbCommand DBCmd = new SqlCommand();
                DBCmd.CommandText = "getAddress";
                DBCmd.CommandType = CommandType.StoredProcedure;
                DBHelper.AddInParameter(DBCmd, "@TCCode", DbType.String, TCCode);
                IDataReader reader = DBHelper.ExecuteReader(DBCmd);
                while (reader.Read())
                {
                    obj.sAddName = Convert.ToString(reader["sAddName"]).Trim();
                    obj.sCity = Convert.ToString(reader["sCity"]).Trim();
                    obj.sCountry = Convert.ToString(reader["sCountry"]).Trim();
                    obj.sAddLine1 = Convert.ToString(reader["sAddLine1"]).Trim();
                    obj.sAddLine2 = Convert.ToString(reader["sAddLine2"]).Trim();
                    obj.sState = Convert.ToString(reader["sState"]).Trim();
                    obj.sZip = Convert.ToString(reader["sZip"]).Trim();
                    obj.sEmailId = Convert.ToString(reader["sEmailId"]).Trim();
                    obj.sAgencyPhn = Convert.ToString(reader["sAgencyPhn"]).Trim();
                    obj.sComments = Convert.ToString(reader["sComments"]).Trim();
                    obj.pay_type = Convert.ToString(reader["pay_type"]).Trim();
                    obj.sFax = Convert.ToString(reader["sFax"]).Trim();
                    obj.sCurrency = Convert.ToString(reader["sCurrency"]).Trim();
                    obj.cityCode = Convert.ToString(reader["cityCode"]).Trim();
                    obj.Mobile = Convert.ToString(reader["Mobile"]).Trim();
                    obj.GSTNumber = Convert.ToString(reader["GSTNumber"]).Trim();
                }
                reader.Close();
                DBCmd.Dispose();
            }
            catch (Exception ex) { ExecptionLogger.FileHandling("DAL_FlightCommanDAL(getAddress)", "Error_007", ex, "Flight"); }
            return obj;
        }

		 #region Seat
        public int InsertSeat(Seat SeatDetails,string fullname)
        {
            int result = 0;
            try
            {


                SqlCommand cmd = new SqlCommand("USP_Seat_Operations");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@PaxId", SeatDetails.PaxId);
                cmd.Parameters.AddWithValue("@OrderId", SeatDetails.OrderId);
                cmd.Parameters.AddWithValue("@SeatDesignator", SeatDetails.SeatDesignator);
                cmd.Parameters.AddWithValue("@Amount", SeatDetails.Amount);
                cmd.Parameters.AddWithValue("@VC", SeatDetails.VC);
                cmd.Parameters.AddWithValue("@Origin", SeatDetails.Origin);
                cmd.Parameters.AddWithValue("@Destination", SeatDetails.Destination);
                cmd.Parameters.AddWithValue("@FlightNumber", SeatDetails.FlightNumber);
                cmd.Parameters.AddWithValue("@FlightTime", SeatDetails.FlightTime);
                cmd.Parameters.AddWithValue("@SeatAlignment", SeatDetails.SeatAlignment);

                cmd.Parameters.AddWithValue("@Ref", SeatDetails.OptionalServiceRef);
                cmd.Parameters.AddWithValue("@Paid", SeatDetails.Paid);
                cmd.Parameters.AddWithValue("@Carrier", SeatDetails.Carrier);
                cmd.Parameters.AddWithValue("@ClassOfService", SeatDetails.ClassOfService);
                cmd.Parameters.AddWithValue("@Equipment", SeatDetails.Equipment);
                cmd.Parameters.AddWithValue("@GroupNo", SeatDetails.Group);
                cmd.Parameters.AddWithValue("@fullname", fullname);
                cmd.Parameters.AddWithValue("@Operation", "INSERT");
                result = DBHelper.ExecuteNonQuery(cmd);
                cmd.Dispose();

            }
            catch (Exception ex)
            {
                ExecptionLogger.FileHandling("DAL_FlightCommanDAL(InsertSeat)", "Error_007", ex, "Flight");
                return result = 0;
            }
            return result;
        }
        public List<Seat> SeatDetails(string OrderId)
        {

            DataTable dt = new DataTable();
            DbCommand cmd = new SqlCommand();
            List<Seat> list = new List<Seat>();
            try
            {
                cmd.CommandText = "USP_Seat_Operations";
                cmd.CommandType = CommandType.StoredProcedure;
                DBHelper.AddInParameter(cmd, "@OrderId", DbType.String, OrderId);
                DBHelper.AddInParameter(cmd, "@Operation", DbType.String, "SELECT");
                dt.Load(DBHelper.ExecuteReader(cmd));


                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    list.Add(new Seat
                    {
                        Counter = Convert.ToInt32(dt.Rows[i]["Counter"]),
                        PaxId = Convert.ToInt32(dt.Rows[i]["PaxId"]),
                        OrderId = Convert.ToString(dt.Rows[i]["OrderId"]),
                        SeatDesignator = Convert.ToString(dt.Rows[i]["SeatDesignator"]),
                        Origin = Convert.ToString(dt.Rows[i]["Origin"]),
                        Destination = Convert.ToString(dt.Rows[i]["Destination"]),
                        VC = Convert.ToString(dt.Rows[i]["VC"]),
                        CreatedDate = Convert.ToString(dt.Rows[i]["CreatedDate"]),
                        FlightNumber = Convert.ToString(dt.Rows[i]["FlightNumber"]),
                        FlightTime = Convert.ToString(dt.Rows[i]["FlightTime"]),
                        Amount = Convert.ToInt32(dt.Rows[i]["Amount"]),
                        SeatAlignment = Convert.ToString(dt.Rows[i]["SeatAlignment"]),
                        OptionalServiceRef = Convert.ToString(dt.Rows[i]["Ref"]),
                        Paid = Convert.ToBoolean(dt.Rows[i]["Paid"]),
                        Carrier = Convert.ToString(dt.Rows[i]["Carrier"]),
                        ClassOfService = Convert.ToString(dt.Rows[i]["ClassOfService"]),
                        Equipment = Convert.ToString(dt.Rows[i]["Equipment"]),
                        Group = Convert.ToString(dt.Rows[i]["GroupNo"]),
                        IsSeat = Convert.ToBoolean(dt.Rows[i]["IsSeat"]),
                        SeatStatus = Convert.ToString(dt.Rows[i]["SeatStatus"])
                    });
                }
                cmd.Dispose();
            }
            catch (Exception ex) { ExecptionLogger.FileHandling("DAL_FlightCommanDAL(GetFareTypeSettings)", "Error_007", ex, "Flight" + OrderId + ""); }
            return list;
        }
        public int Update_NET_TOT_Fare_Seat(string TrackId, string totmealBagPrice)
        {
            int status = 0;
            try
            {
                SqlCommand cmd = new SqlCommand("Update_Tot_Net_Fare_V1");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@oid", TrackId);
                cmd.Parameters.AddWithValue("@Amount", totmealBagPrice);
                cmd.Parameters.AddWithValue("@OPS", "Seat");
                status = DBHelper.ExecuteNonQuery(cmd);
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                ExecptionLogger.FileHandling("DAL_FlightCommanDAL(Update_NET_TOT_Fare)", "Error_007", ex, "Flight");
            }

            return status;

        }
   
        public int Update_Seat_Details(int Counter, string SeatStatus, bool IsSeatAssignment)
        {
            int status = 0;
            try
            {
                SqlCommand cmd = new SqlCommand("USP_Seat_Operations");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@PaxId", Counter);
                cmd.Parameters.AddWithValue("@SeatAlignment", SeatStatus);
                cmd.Parameters.AddWithValue("@Paid", IsSeatAssignment);
                cmd.Parameters.AddWithValue("@Operation", "UPDATE");
                status = DBHelper.ExecuteNonQuery(cmd);
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                ExecptionLogger.FileHandling("DAL_FlightCommanDAL(Update_Seat_Details)", "Error_007", ex, "Flight");
            }

            return status;

        }
        #endregion


        public List<FlightFareType> GetFlightFareTypeMaster(string FareType, string ActionType)
        {

            DataTable dt = new DataTable();
            DbCommand cmd = new SqlCommand();
            List<FlightFareType> list = new List<FlightFareType>();
            try
            {
                cmd.CommandText = "USP_GET_FARETYPEMASTER";
                cmd.CommandType = CommandType.StoredProcedure;
                DBHelper.AddInParameter(cmd, "@FareType", DbType.String, FareType);
                DBHelper.AddInParameter(cmd, "@ActionType", DbType.String, ActionType);                
                dt.Load(DBHelper.ExecuteReader(cmd));
                if (dt != null && dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        list.Add(new FlightFareType
                        {
                            FareType = Convert.ToString(dt.Rows[i]["FareType"]),
                            DisplayName = Convert.ToString(dt.Rows[i]["DisplayName"]),
                            Status = Convert.ToBoolean(dt.Rows[i]["Status"])
                        });
                    }
                }
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                cmd.Dispose();
                ITZERRORLOG.ExecptionLogger.FileHandling("FlightCommonDAL(FlightCommonDAL_GetFlightFareTypeMaster)", "Error_001", ex, "GetFlightFareTypeMaster");
            }
            return list;
        }

    }
}
