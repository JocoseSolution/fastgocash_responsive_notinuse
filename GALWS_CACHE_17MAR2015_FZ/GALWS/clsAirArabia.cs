﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Text;
using System.Net;
using System.IO;
using System.IO.Compression;
using System.Xml;
using System.Xml.Linq;

/// <summary>
/// Summary description for Class1
/// </summary>
namespace AirArabia
{
    public class clsAirArabia
    {

        public string getAvailabilty(FltSearch obj)
        {

            DataTable table = new DataTable();
            string sUserId = "WSSPRINGT";
            string sPassword = "password123";
            FltSearch ObjFltSearch = new FltSearch();

            string xml = pxml(ObjFltSearch);
            string sResponse = "";


            sResponse = PostXml("http://airarabia.isaaviations.com/webservices/services/AAResWebServicesForBBR", xml);

            return sResponse;


        }



        public string PostXml(string url, string xml)
        {
            StringBuilder sbResult = new StringBuilder();
            try
            {
                HttpWebRequest Http = (HttpWebRequest)WebRequest.Create(url);
                if (!string.IsNullOrEmpty(xml))
                {
                    Http.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip,deflate");
                    Http.Method = "POST";
                    byte[] lbPostBuffer = Encoding.UTF8.GetBytes(xml);
                    Http.ContentLength = lbPostBuffer.Length;
                    Http.ContentType = "text/xml";
                    using (Stream PostStream = Http.GetRequestStream())
                    {
                        PostStream.Write(lbPostBuffer, 0, lbPostBuffer.Length);
                    }
                }

                using (HttpWebResponse WebResponse = (HttpWebResponse)Http.GetResponse())
                {
                    if (WebResponse.StatusCode != HttpStatusCode.OK)
                    {
                        string message = String.Format("POST failed. Received HTTP {0}", WebResponse.StatusCode);
                        throw new ApplicationException(message);
                    }
                    else
                    {
                        Stream responseStream = WebResponse.GetResponseStream();
                        if ((WebResponse.ContentEncoding.ToLower().Contains("gzip")))
                        {
                            responseStream = new GZipStream(responseStream, CompressionMode.Decompress);
                        }
                        else if ((WebResponse.ContentEncoding.ToLower().Contains("deflate")))
                        {
                            responseStream = new DeflateStream(responseStream, CompressionMode.Decompress);
                        }
                        StreamReader reader = new StreamReader(responseStream, Encoding.Default);
                        sbResult.Append(reader.ReadToEnd());
                        responseStream.Close();

                       // bool x = SaveTextToFile(sbResult.ToString(), @"D:/packagescollection/ " + ".xml");
                    }
                }
            }
            catch (WebException ex)
            {
                WebResponse wre = ex.Response;
                Stream st = wre.GetResponseStream();
                StreamReader readers = new StreamReader(st, Encoding.Default);
                sbResult.Append(readers.ReadToEnd());
            }
            return sbResult.ToString();



        }
      


        public string pxml(FltSearch ObjFltSearch)
        {
            StringBuilder sb = new StringBuilder();
            string xyz;

            sb.Append(string.Format("<?xml version='1.0' encoding='utf-8'?>"));
            sb.Append(string.Format("<soap:Envelope xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'>"));
            sb.Append(string.Format("<soap:Header>"));
            sb.Append(string.Format("<wsse:Security soap:mustUnderstand='1' xmlns:wsse='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd'>"));
            sb.Append(string.Format("<wsse:UsernameToken wsu:Id='UsernameToken-17855236' xmlns:wsu='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd'>"));
            sb.Append(string.Format("<wsse:Username>WSSPRINGT</wsse:Username>"));
            sb.Append(string.Format("<wsse:Password Type='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText'>password123</wsse:Password>"));
            sb.Append(string.Format("</wsse:UsernameToken>"));
            sb.Append(string.Format("</wsse:Security>"));
            sb.Append(string.Format("</soap:Header>"));
            sb.Append(string.Format("<soap:Body xmlns:ns2='http://www.opentravel.org/OTA/2003/05'>"));
            sb.Append(string.Format("<ns2:OTA_AirAvailRQ EchoToken='11868765275150-1300257933' PrimaryLangID='en-us' SequenceNmbr='1' Target='TEST' TimeStamp='2011-01-12T17:15:04' Version='20061.00'>"));
            sb.Append(string.Format("<ns2:POS>"));
            sb.Append(string.Format("<ns2:Source TerminalID='TestUser/Test Runner'>"));
            sb.Append(string.Format("<ns2:RequestorID ID='WSSPRINGT' Type='4' />"));
            sb.Append(string.Format("<ns2:BookingChannel Type='12' />"));
            sb.Append(string.Format("</ns2:Source>"));
            sb.Append(string.Format("</ns2:POS>"));
            sb.Append(string.Format("<ns2:OriginDestinationInformation>"));
            sb.Append(string.Format("<ns2:DepartureDateTime>" + ObjFltSearch.DepartureDateTime + "</ns2:DepartureDateTime>"));
            sb.Append(string.Format("<ns2:OriginLocation LocationCode='" + ObjFltSearch.OriginLocation + "' />"));
            sb.Append(string.Format("<ns2:DestinationLocation LocationCode='" + ObjFltSearch.DestinationLocation + "' />"));

            sb.Append(string.Format("</ns2:OriginDestinationInformation>"));

            xyz = sb.ToString();


            bool oneway = true;
            if (oneway == true)
            {
                xyz = sb.ToString();
                sb.Append(string.Format("<ns2:TravelerInfoSummary>"));
                sb.Append(string.Format("<ns2:AirTravelerAvail>"));
                sb.Append(string.Format("<ns2:PassengerTypeQuantity Code='ADT'  Quantity='" + ObjFltSearch.dTotalNo_Adt + "'/>"));
                sb.Append(string.Format("<ns2:PassengerTypeQuantity Code='CHD'  Quantity='" + ObjFltSearch.dTotalNo_CHD + "'/>"));
                sb.Append(string.Format("<ns2:PassengerTypeQuantity Code='INF'  Quantity='" + ObjFltSearch.dTotalNo_INF + "'/>"));
                sb.Append(string.Format("</ns2:AirTravelerAvail>"));
                sb.Append(string.Format("</ns2:TravelerInfoSummary>"));
                sb.Append(string.Format("</ns2:OTA_AirAvailRQ>"));
                sb.Append(string.Format("</soap:Body>"));
                sb.Append(string.Format("</soap:Envelope>"));
                sb.ToString();

                return sb.ToString();
            }
            else
            {
                xyz = sb.ToString();

                sb.Append(string.Format("<ns2:OriginDestinationInformation>"));
                sb.Append(string.Format("<ns2:DepartureDateTime>" + ObjFltSearch.ArrivalDateTime + "</ns2:DepartureDateTime>"));
                sb.Append(string.Format("<ns2:OriginLocation LocationCode= '" + ObjFltSearch.DestinationLocation + "'/>"));
                sb.Append(string.Format("<ns2:DestinationLocation LocationCode='" + ObjFltSearch.OriginLocation + "'/>"));

                sb.Append(string.Format("</ns2:OriginDestinationInformation>"));
                sb.Append(string.Format(" <ns2:TravelerInfoSummary>"));
                sb.Append(string.Format("<ns2:AirTravelerAvail>"));

                sb.Append(string.Format("<ns2:PassengerTypeQuantity Code='ADT'  Quantity='" + ObjFltSearch.dTotalNo_Adt + "'/>"));
                sb.Append(string.Format("<ns2:PassengerTypeQuantity Code='CHD'  Quantity='" + ObjFltSearch.dTotalNo_CHD + "'/>"));
                sb.Append(string.Format("<ns2:PassengerTypeQuantity Code='INF'  Quantity='" + ObjFltSearch.dTotalNo_INF + "'/>"));

                sb.Append(string.Format("</ns2:AirTravelerAvail>"));
                sb.Append(string.Format("</ns2:TravelerInfoSummary>"));
                sb.Append(string.Format("</ns2:OTA_AirAvailRQ>"));
                sb.Append(string.Format("</soap:Body>"));
                sb.Append(string.Format("</soap:Envelope>"));


                return sb.ToString();





            }

        }



        public string booking(Request_Input ObjReqInput)
        {



            StringBuilder sb = new StringBuilder();
            sb.Append(string.Format("<?xml version='1.0' encoding='utf-8' ?>"));
            sb.Append(string.Format("<soap:Envelope xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'>"));
            sb.Append(string.Format("<soap:Header>"));
            sb.Append(string.Format("<wsse:Security soap:mustUnderstand='1' xmlns:wsse='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd'>"));
            sb.Append(string.Format("<wsse:UsernameToken wsu:Id='UsernameToken-17855236' xmlns:wsu='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd'>"));
            sb.Append(string.Format("<wsse:Username>WSSPRING</wsse:Username>"));
            sb.Append(string.Format("<wsse:Password Type='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText'>password123</wsse:Password>"));
            sb.Append(string.Format("</wsse:UsernameToken>"));
            sb.Append(string.Format("</wsse:Security>"));
            sb.Append(string.Format("</soap:Header>"));
            sb.Append(string.Format("<soap:Body xmlns:ns1='http://www.isaaviation.com/thinair/webservices/OTA/Extensions/2003/05' xmlns:ns2='http://www.opentravel.org/OTA/2003/05'>"));
            sb.Append(string.Format("<ns2:OTA_AirBookRQ EchoToken='11868765275150-1300257933' PrimaryLangID='en-us' SequenceNmbr='1' TimeStamp='2011-01-12T17:15:04' TransactionIdentifier='TID$13557479851241631.g9_node2' Version='20061.00'>"));
            sb.Append(string.Format("<ns2:POS>"));
            sb.Append(string.Format("<ns2:Source TerminalID='TestUser/Test Runner'>"));
            sb.Append(string.Format("<ns2:RequestorID ID='WSSPRINGT' Type='4' />"));
            sb.Append(string.Format("<ns2:BookingChannel Type='12' />"));
            sb.Append(string.Format("</ns2:Source>"));
            sb.Append(string.Format("</ns2:POS>"));
            sb.Append(string.Format("<ns2:AirItinerary>"));
            sb.Append(string.Format("<ns2:OriginDestinationOptions>"));
            sb.Append(string.Format("<ns2:OriginDestinationOption>"));
            sb.Append(string.Format("<ns2:FlightSegment ArrivalDateTime='" + ObjReqInput.ArrivalDateTime + "' DepartureDateTime='" + ObjReqInput.DepartureDateTime + "' FlightNumber='" + ObjReqInput.FlightNumber + "' RPH='" + ObjReqInput.RPH + "'>"));
            sb.Append(string.Format("<ns2:DepartureAirport LocationCode='" + ObjReqInput.OriginLocation + "' Terminal='TerminalX' />"));
            sb.Append(string.Format("<ns2:ArrivalAirport LocationCode='" + ObjReqInput.DestinationLocation + "' Terminal='TerminalX' />"));
            sb.Append(string.Format("</ns2:FlightSegment>"));
            sb.Append(string.Format("</ns2:OriginDestinationOption>"));
            sb.Append(string.Format("</ns2:OriginDestinationOptions>"));
            sb.Append(string.Format("</ns2:AirItinerary>"));
            sb.Append(string.Format("<ns2:TravelerInfo>"));
            sb.Append(string.Format("<ns2:AirTraveler BirthDate='" + ObjReqInput.BirthDate + "' PassengerTypeCode='ADT'>"));
            sb.Append(string.Format(" <ns2:PersonName>"));
            sb.Append(string.Format("<ns2:GivenName>Testa</ns2:GivenName>"));
            sb.Append(string.Format("<ns2:Surname>" + ObjReqInput.Surname + "</ns2:Surname> "));
            sb.Append(string.Format("<ns2:NameTitle>" + ObjReqInput.NameTitle + "</ns2:NameTitle>"));
            sb.Append(string.Format("</ns2:PersonName>"));
            sb.Append(string.Format("<ns2:Telephone AreaCityCode='" + ObjReqInput.AreaCityCode + "' CountryAccessCode='" + ObjReqInput.CountryAccessCode + "' PhoneNumber='" + ObjReqInput.PhoneNumber + "' />"));
            sb.Append(string.Format("<ns2:Address>"));
            sb.Append(string.Format("<ns2:CountryName Code='" + ObjReqInput.CountryNameCode + "' />"));
            sb.Append(string.Format("</ns2:Address>"));
            sb.Append(string.Format("<ns2:Document DocHolderNationality='" + ObjReqInput.DocHolderNationality + "' />"));
            sb.Append(string.Format("<ns2:TravelerRefNumber RPH='" + ObjReqInput.RPH + "' />"));
            sb.Append(string.Format("</ns2:AirTraveler>"));
            //sb.Append(string.Format("<ns2:AirTraveler BirthDate='1976-08-12T00:00:00' PassengerTypeCode='ADT'>"));
            //sb.Append(string.Format("<ns2:PersonName>"));
            // sb.Append(string.Format("<ns2:GivenName>Testa</ns2:GivenName>")); 
            // sb.Append(string.Format("<ns2:Surname>test</ns2:Surname> "));
            // sb.Append(string.Format("<ns2:NameTitle>MR</ns2:NameTitle> "));
            // sb.Append(string.Format("</ns2:PersonName>"));
            // sb.Append(string.Format("<ns2:Telephone AreaCityCode='11' CountryAccessCode='91' PhoneNumber='1234567' /> - <ns2:Address>"));
            // sb.Append(string.Format("<ns2:CountryName Code='IN' />")); 
            // sb.Append(string.Format("</ns2:Address>"));
            // sb.Append(string.Format("<ns2:Document DocHolderNationality='IN' />")); 
            // sb.Append(string.Format("<ns2:TravelerRefNumber RPH='A2' />")); 
            // sb.Append(string.Format("</ns2:AirTraveler>"));
            sb.Append(string.Format("</ns2:TravelerInfo>"));
            sb.Append(string.Format("<ns2:Fulfillment>"));
            sb.Append(string.Format("<ns2:PaymentDetails>"));
            sb.Append(string.Format("<ns2:PaymentDetail>"));
            sb.Append(string.Format("<ns2:DirectBill>"));
            sb.Append(string.Format("<ns2:CompanyName Code='SHJ462' />"));
            sb.Append(string.Format("</ns2:DirectBill>"));
            sb.Append(string.Format("<ns2:PaymentAmount Amount='790.00' CurrencyCode='AED' DecimalPlaces='2' />"));
            sb.Append(string.Format("</ns2:PaymentDetail>"));
            sb.Append(string.Format("</ns2:PaymentDetails>"));
            sb.Append(string.Format("</ns2:Fulfillment>"));
            sb.Append(string.Format("</ns2:OTA_AirBookRQ>"));
            sb.Append(string.Format("<ns1:AAAirBookRQExt>"));
            sb.Append(string.Format("<ns1:ContactInfo>"));
            sb.Append(string.Format("<ns1:PersonName>"));
            sb.Append(string.Format("<ns1:Title>MR</ns1:Title>"));
            sb.Append(string.Format("<ns1:FirstName>Testa</ns1:FirstName>"));
            sb.Append(string.Format("<ns1:LastName>test</ns1:LastName> "));
            sb.Append(string.Format("</ns1:PersonName>"));
            sb.Append(string.Format("<ns1:Telephone>"));
            sb.Append(string.Format("<ns1:PhoneNumber>1234567</ns1:PhoneNumber>"));
            sb.Append(string.Format("<ns1:CountryCode>91</ns1:CountryCode> "));
            sb.Append(string.Format("<ns1:AreaCode>11</ns1:AreaCode> "));
            sb.Append(string.Format("</ns1:Telephone>"));
            sb.Append(string.Format("<ns1:Email>info@abc.com</ns1:Email>"));
            sb.Append(string.Format("<ns1:Address>"));
            sb.Append(string.Format("<ns1:CountryName>"));
            sb.Append(string.Format("<ns1:CountryName>India</ns1:CountryName>"));
            sb.Append(string.Format("<ns1:CountryCode>IN</ns1:CountryCode> "));
            sb.Append(string.Format("</ns1:CountryName>"));
            sb.Append(string.Format("<ns1:CityName>New Delhi</ns1:CityName>"));
            sb.Append(string.Format("</ns1:Address>"));
            sb.Append(string.Format("</ns1:ContactInfo>"));
            sb.Append(string.Format("</ns1:AAAirBookRQExt>"));
            sb.Append(string.Format("</soap:Body>"));
            sb.Append(string.Format("</soap:Envelope>"));
            sb.ToString();
            PostXml("http://airarabia.isaaviations.com/webservices/services/AAResWebServicesForBBR", sb.ToString());
            return sb.ToString();













        }



        public string PriceQuote(Request_Input ObjReqInput)
        {

            StringBuilder sb = new StringBuilder();
            sb.Append(string.Format("<?xml version='1.0' encoding='utf-8' ?>"));
            sb.Append(string.Format("<soap:Envelope xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'>"));
            sb.Append(string.Format("<soap:Header>"));
            sb.Append(string.Format("<wsse:Security soap:mustUnderstand='1' xmlns:wsse='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd'>"));
            sb.Append(string.Format("<wsse:UsernameToken wsu:Id='UsernameToken-32124385' xmlns:wsu='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd'>"));
            sb.Append(string.Format("<wsse:Username>WSSPRINGT</wsse:Username>"));
            sb.Append(string.Format("<wsse:Password Type='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText'>password123</wsse:Password>"));
            sb.Append(string.Format("</wsse:UsernameToken>"));
            sb.Append(string.Format("</wsse:Security>"));
            sb.Append(string.Format("</soap:Header>"));
            sb.Append(string.Format("<soap:Body xmlns:ns1='http://www.opentravel.org/OTA/2003/05'>"));
            sb.Append(string.Format("<ns1:OTA_AirPriceRQ EchoToken='" + ObjReqInput.EchoToken + "' PrimaryLangID='en-us' SequenceNmbr='1' TimeStamp='2012-21-12T11:25:00' TransactionIdentifier='" + ObjReqInput.TransactionIdentifier + "' Version='" + ObjReqInput.Version + "'>"));
            sb.Append(string.Format("<ns1:POS>"));
            sb.Append(string.Format("<ns1:Source TerminalID='TestUser/Test Runner'>"));
            sb.Append(string.Format("<ns1:RequestorID ID='WSSPRINGT' Type='4'/>"));
            sb.Append(string.Format("<ns1:BookingChannel Type='12' />"));
            sb.Append(string.Format("</ns1:Source>"));
            sb.Append(string.Format("</ns1:POS>"));
            sb.Append(string.Format("<ns1:AirItinerary DirectionInd='" + ObjReqInput.TripType + "'>"));
            sb.Append(string.Format("<ns1:OriginDestinationOptions>"));
            for (int i = 0; i < 2; i++)
            {

                sb.Append(string.Format("<ns1:OriginDestinationOption>"));
                sb.Append(string.Format("<ns1:FlightSegment ArrivalDateTime='" + ObjReqInput.ArrivalDateTime + "' DepartureDateTime='" + ObjReqInput.DepartureDateTime + "' FlightNumber='" + ObjReqInput.FlightNumber + "' RPH='" + ObjReqInput.RPH + "'>"));
                sb.Append(string.Format("<ns1:DepartureAirport LocationCode='" + ObjReqInput.OriginLocation + "' Terminal='TerminalX'/>"));
                sb.Append(string.Format("<ns1:ArrivalAirport LocationCode='" + ObjReqInput.DestinationLocation + "' Terminal='TerminalX'/>"));
                sb.Append(string.Format("</ns1:FlightSegment>"));
                sb.Append(string.Format("</ns1:OriginDestinationOption>"));

            }

            sb.Append(string.Format("</ns1:OriginDestinationOptions>"));
            sb.Append(string.Format("</ns1:AirItinerary>"));
            sb.Append(string.Format(" <ns1:TravelerInfoSummary>"));
            sb.Append(string.Format("<ns1:AirTravelerAvail>"));
            sb.Append(string.Format("<ns1:PassengerTypeQuantity Code='ADT' Quantity='" + ObjReqInput.dTotalNo_Adt + "'/>"));
            sb.Append(string.Format("<ns1:PassengerTypeQuantity Code='CHD' Quantity='" + ObjReqInput.dTotalNo_CHD + "'/>"));
            sb.Append(string.Format("<ns1:PassengerTypeQuantity Code='INF' Quantity='" + ObjReqInput.dTotalNo_INF + "'/>"));
            sb.Append(string.Format("</ns1:AirTravelerAvail>"));
            sb.Append(string.Format("</ns1:TravelerInfoSummary>"));
            sb.Append(string.Format("</ns1:OTA_AirPriceRQ>"));
            sb.Append(string.Format("</soap:Body>"));
            sb.Append(string.Format("</soap:Envelope>"));

            string result = PostXml("http://airarabia.isaaviations.com/webservices/services/AAResWebServicesForBBR", sb.ToString());
            return result.ToString();


        }










    }
}
