﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Data;

namespace GALWS.ITQAirApi
{
    public class ITQAirApiUtility
    {
        public static string Authenticate(string url, string userid, string password)
        {
            string request = "{\"UserName\": \"" + userid + "\",  \"Password\": \"" + password + "\"}";
            string response = PostJsionXML(url, "application/json; charset=utf-8", request);


            string[] sessions = response.Split(':');
            string session = sessions[1].Split(',')[0];
            return session.Replace("\"", "");
        }
        public static string PostJsionXML(string url, string AcceptType, string requestData)
        {
            string responseXML = string.Empty;
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                byte[] data = Encoding.UTF8.GetBytes(requestData);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";
                request.Accept = AcceptType;
                request.Headers.Add("Accept-Encoding", "gzip");
                request.ReadWriteTimeout = 200000;
                request.Timeout = 200000;
                Stream dataStream = request.GetRequestStream();
                dataStream.Write(data, 0, data.Length);
                dataStream.Close();
                HttpWebResponse webResponse = (HttpWebResponse)request.GetResponse();
                var rsp = webResponse.GetResponseStream();
                if (rsp == null)
                {
                    //throw exception
                }
                if ((webResponse.ContentEncoding.ToLower().Contains("gzip")))
                {
                    using (StreamReader readStream = new StreamReader(new GZipStream(rsp, CompressionMode.Decompress)))
                    {
                        responseXML = readStream.ReadToEnd();
                    }
                }
                else
                {
                    StreamReader reader = new StreamReader(rsp, Encoding.Default);
                    responseXML = reader.ReadToEnd();
                }
            }
            catch (WebException webEx)
            {
                if (webEx != null)
                {
                    WebResponse response = webEx.Response;
                    Stream stream = response.GetResponseStream();
                    responseXML = new StreamReader(stream).ReadToEnd();
                }
                else
                    responseXML = "<Errors>" + webEx.Message + "</Errors>";
            }
            catch (Exception ex)
            {
                responseXML = "<Errors>" + ex.Message + "</Errors>";
            }
            return responseXML;
        }

        public static void SaveFile(string Res, string module, string Userid, string ailine = "")
        {

            string Hour = DateTime.Now.ToString("HH");
            string Minutes = DateTime.Now.ToString("mm");
            string newFileName = module + DateTime.Now.ToString("hh_mm_ss");
            string activeDir = ConfigurationManager.AppSettings["ItqAirApiSaveUrl"] + DateTime.Now.ToString("dd-MMMM-yyyy") + "\\" + Userid + "\\";
            if (newFileName.Contains("SearchReq_") || newFileName.Contains("SearchRes_") || newFileName.Contains("ItineraryPriceReq_") || newFileName.Contains("ItineraryPriceRes_") || newFileName.Contains("FareRulesRequest_") || newFileName.Contains("FareRulesResponse_"))
                activeDir += Hour + "\\" + ailine + "\\";
            else
            {
                string[] orderid = module.Split('_');
                activeDir += @"\" + orderid[1] + @"\";
                newFileName = newFileName.Replace(orderid[1], "");
            }

            DirectoryInfo objDirectoryInfo = new DirectoryInfo(activeDir);
            if (!Directory.Exists(objDirectoryInfo.FullName))
            {
                Directory.CreateDirectory(activeDir);
            }
            try
            {
                string path = activeDir + newFileName + ".txt";
                if (!File.Exists(path))
                {
                    // Create a file to write to.
                    using (StreamWriter sw = File.CreateText(path))
                    {
                        sw.Write(Res);
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }
    }
}
