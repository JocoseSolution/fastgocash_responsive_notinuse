﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace STD.BAL
{
    public class G8SvcAndMethodUrls
    {
        //public string RetrieveSecurityTokenUrl { get { return "http://tempuri.org/IConnectPoint_Security/RetrieveSecurityToken"; } }
        //// public string SecurityTokenSvcUrl { get { return "http://G8.connectpoint.radixx.com/ConnectPoint.Security.svc"; } }        
        //public string SecurityTokenSvcUrl { get { return "http://G8.connectpoint.uat.radixx.com/ConnectPoint.Security.svc"; } }

        //public string TASvcUrl { get { return "http://G8.connectpoint.uat.radixx.com/ConnectPoint.TravelAgents.svc"; } }

        //// public string TASvcUrl { get { return "http://G8.connectpoint.radixx.com/ConnectPoint.TravelAgents.svc "; } }
        //public string LoginTAUrl { get { return "http://tempuri.org/IConnectPoint_TravelAgents/LoginTravelAgent"; } }
        //public string TransFeeTAUrl { get { return "http://tempuri.org/IConnectPoint_TravelAgents/RetrieveAgencyCommission"; } }


        //public string PricingSvcUrl { get { return "http://G8.connectpoint.uat.radixx.com/ConnectPoint.Pricing.svc"; } }

        ////public string PricingSvcUrl { get { return "http://G8.connectpoint.radixx.com/ConnectPoint.Pricing.svc"; } }
        //public string RetrieveFareQuotePRUrl { get { return "http://tempuri.org/IConnectPoint_Pricing/RetrieveFareQuote"; } }
        //public string SSRUrl { get { return "http://tempuri.org/IConnectPoint_Pricing/RetrieveServiceQuotes"; } }


        //public string ReservationSvcUrl { get { return "http://G8.connectpoint.uat.radixx.com/ConnectPoint.Reservation.svc"; } }

        //// public string ReservationSvcUrl { get { return "http://G8.connectpoint.radixx.com/ConnectPoint.Reservation.svc "; } }
        //public string SummaryPNRUrl { get { return "http://tempuri.org/IConnectPoint_Reservation/SummaryPNR"; } }
        //public string CreatePNRUrl { get { return "http://tempuri.org/IConnectPoint_Reservation/CreatePNR"; } }
        //public string RetrievePNRUrl { get { return "http://tempuri.org/IConnectPoint_Reservation/RetrievePNR"; } }
        //public string CancelPNRUrl { get { return "http://tempuri.org/IConnectPoint_Reservation/CancelPNR"; } }

        //public string FulfillmentSvcUrl { get { return "http://G8.connectpoint.uat.radixx.com/ConnectPoint.Fulfillment.svc"; } }

        //// public string FulfillmentSvcUrl { get { return "http://G8.connectpoint.radixx.com/ConnectPoint.Fulfillment.svc"; } }
        //public string ProcessPNRPaymentUrl { get { return "http://tempuri.org/IConnectPoint_Fulfillment/ProcessPNRPayment"; } }



        #region Live Service
        public string RetrieveSecurityTokenUrl { get { return "http://tempuri.org/IConnectPoint_Security/RetrieveSecurityToken"; } }
        public string SecurityTokenSvcUrl { get { return "http://G8.connectpoint.radixx.com/ConnectPoint.Security.svc"; } }

        public string TASvcUrl { get { return "http://G8.connectpoint.radixx.com/ConnectPoint.TravelAgents.svc"; } }
        public string LoginTAUrl { get { return "http://tempuri.org/IConnectPoint_TravelAgents/LoginTravelAgent"; } }
        public string TransFeeTAUrl { get { return "http://tempuri.org/IConnectPoint_TravelAgents/RetrieveAgencyCommission"; } }


        public string PricingSvcUrl { get { return "http://G8.connectpoint.radixx.com/ConnectPoint.Pricing.svc"; } }
        public string RetrieveFareQuotePRUrl { get { return "http://tempuri.org/IConnectPoint_Pricing/RetrieveFareQuote"; } }
        public string SSRUrl { get { return "http://tempuri.org/IConnectPoint_Pricing/RetrieveServiceQuotes"; } }


        public string ReservationSvcUrl { get { return "http://G8.connectpoint.radixx.com/ConnectPoint.Reservation.svc"; } }
        public string SummaryPNRUrl { get { return "http://tempuri.org/IConnectPoint_Reservation/SummaryPNR"; } }
        public string CreatePNRUrl { get { return "http://tempuri.org/IConnectPoint_Reservation/CreatePNR"; } }
        public string RetrievePNRUrl { get { return "http://tempuri.org/IConnectPoint_Reservation/RetrievePNR"; } }
        public string CancelPNRUrl { get { return "http://tempuri.org/IConnectPoint_Reservation/CancelPNR"; } }

        public string FulfillmentSvcUrl { get { return "http://G8.connectpoint.radixx.com/ConnectPoint.Fulfillment.svc"; } }
        public string ProcessPNRPaymentUrl { get { return "http://tempuri.org/IConnectPoint_Fulfillment/ProcessPNRPayment"; } }
        #endregion

    }

    public class IXSvcAndMethodUrls
    {

        #region IX  Test URL
        //public string RetrieveSecurityTokenUrl { get { return "http://tempuri.org/IConnectPoint_Security/RetrieveSecurityToken"; } }
        //public string SecurityTokenSvcUrl { get { return "http://IX.connectpoint.uat.radixx.com/ConnectPoint.Security.svc"; } }
        //public string TASvcUrl { get { return "http://IX.connectpoint.uat.radixx.com/ConnectPoint.TravelAgents.svc"; } }
        //public string LoginTAUrl { get { return "http://tempuri.org/IConnectPoint_TravelAgents/LoginTravelAgent"; } }
        //public string TransFeeTAUrl { get { return "http://tempuri.org/IConnectPoint_TravelAgents/RetrieveAgencyCommission"; } }
        //public string PricingSvcUrl { get { return "http://IX.connectpoint.uat.radixx.com/ConnectPoint.Pricing.svc"; } }
        //public string RetrieveFareQuotePRUrl { get { return "http://tempuri.org/IConnectPoint_Pricing/RetrieveFareQuote"; } }
        //public string SSRUrl { get { return "http://tempuri.org/IConnectPoint_Pricing/RetrieveServiceQuotes"; } }
        //public string ReservationSvcUrl { get { return "http://IX.connectpoint.uat.radixx.com/ConnectPoint.Reservation.svc"; } }
        //public string SummaryPNRUrl { get { return "http://tempuri.org/IConnectPoint_Reservation/SummaryPNR"; } }
        //public string CreatePNRUrl { get { return "http://tempuri.org/IConnectPoint_Reservation/CreatePNR"; } }
        //public string RetrievePNRUrl { get { return "http://tempuri.org/IConnectPoint_Reservation/RetrievePNR"; } }
        //public string CancelPNRUrl { get { return "http://tempuri.org/IConnectPoint_Reservation/CancelPNR"; } }
        //public string FulfillmentSvcUrl { get { return "http://IX.connectpoint.uat.radixx.com/ConnectPoint.Fulfillment.svc"; } }
        //public string ProcessPNRPaymentUrl { get { return "http://tempuri.org/IConnectPoint_Fulfillment/ProcessPNRPayment"; } }

        #endregion

        #region Live Service
        public string RetrieveSecurityTokenUrl { get { return "http://tempuri.org/IConnectPoint_Security/RetrieveSecurityToken"; } }
        public string SecurityTokenSvcUrl { get { return "http://ix.connectpoint.radixx.com/ConnectPoint.Security.svc"; } }
        public string TASvcUrl { get { return "http://ix.connectpoint.radixx.com/ConnectPoint.TravelAgents.svc"; } }
        public string LoginTAUrl { get { return "http://tempuri.org/IConnectPoint_TravelAgents/LoginTravelAgent"; } }
        public string TransFeeTAUrl { get { return "http://tempuri.org/IConnectPoint_TravelAgents/RetrieveAgencyCommission"; } }
        public string PricingSvcUrl { get { return "http://ix.connectpoint.radixx.com/ConnectPoint.Pricing.svc"; } }
        public string RetrieveFareQuotePRUrl { get { return "http://tempuri.org/IConnectPoint_Pricing/RetrieveFareQuote"; } }
        public string SSRUrl { get { return "http://tempuri.org/IConnectPoint_Pricing/RetrieveServiceQuotes"; } }
        public string ReservationSvcUrl { get { return "http://ix.connectpoint.radixx.com/ConnectPoint.Reservation.svc"; } }
        public string SummaryPNRUrl { get { return "http://tempuri.org/IConnectPoint_Reservation/SummaryPNR"; } }
        public string CreatePNRUrl { get { return "http://tempuri.org/IConnectPoint_Reservation/CreatePNR"; } }
        public string RetrievePNRUrl { get { return "http://tempuri.org/IConnectPoint_Reservation/RetrievePNR"; } }
        public string CancelPNRUrl { get { return "http://tempuri.org/IConnectPoint_Reservation/CancelPNR"; } }
        public string FulfillmentSvcUrl { get { return "http://ix.connectpoint.radixx.com/ConnectPoint.Fulfillment.svc"; } }
        public string ProcessPNRPaymentUrl { get { return "http://tempuri.org/IConnectPoint_Fulfillment/ProcessPNRPayment"; } }

        #endregion


    }
}

 

 

