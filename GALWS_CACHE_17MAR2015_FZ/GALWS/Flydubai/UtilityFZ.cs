﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.IO.Compression;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Linq;

namespace STD.BAL
{
    public class FZUtility
    {


        public static string PostXml(String XMLIn, String Module_Version, string bkgUrl,ref string exep)
        {
            string rawResponse = string.Empty;
            try
            {
                byte[] bytes = Encoding.UTF8.GetBytes(XMLIn);
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(bkgUrl);
                request.Headers.Add(String.Format("SOAPAction: \"{0}\"", Module_Version));
                request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip,deflate");
                request.Method = "POST";
                request.ContentLength = bytes.Length;
                request.ContentType = @"text/xml;charset=utf-8";
                request.Credentials = CredentialCache.DefaultCredentials;
                request.ProtocolVersion = HttpVersion.Version11;
                request.Timeout = 300000;
                using (Stream requestStream = request.GetRequestStream())
                {
                    requestStream.Write(bytes, 0, bytes.Length);
                }
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        string message = String.Format("POST failed. Received HTTP {0}", response.StatusCode);
                        throw new ApplicationException(message);
                    }
                    rawResponse = GetWebResponseString(response);
                    return rawResponse;
                }
            }
            catch (WebException wex)
            {
                try
                {
                    rawResponse = "Error: " + new StreamReader(wex.Response.GetResponseStream()).ReadToEnd();

                    exep = " PostXML Method (" + Module_Version + ") :" + wex.Message + " StackTrace:" + wex.StackTrace;
                }
                catch { rawResponse = "Error: "; }
            }

            catch (Exception ex)
            {
                //ExceptionLogger objExceptionLogger = new ExceptionLogger(ExceptionLogger.ERROR_SOURCE.Services, ex, "699", rawResponse, string.Empty, "");
                rawResponse = "Error: " + ex.Message + " StackTrace: " + ex.StackTrace;
                exep = exep + " PostXML Method (" + Module_Version + ") :" + ex.Message + " StackTrace: " + ex.StackTrace;
            }
            return rawResponse;
        }



        private static string GetWebResponseString(HttpWebResponse myHttpWebResponse)
        {
            StringBuilder rawResponse = new StringBuilder();
            Stream streamResponse;
            using (streamResponse = myHttpWebResponse.GetResponseStream())
            {
                if (myHttpWebResponse.ContentEncoding.ToLower().Contains("gzip"))
                    streamResponse = new GZipStream(streamResponse, CompressionMode.Decompress);
                else if (myHttpWebResponse.ContentEncoding.ToLower().Contains("deflate"))
                    streamResponse = new DeflateStream(streamResponse, CompressionMode.Decompress);
                using (StreamReader streamRead = new StreamReader(streamResponse))
                {
                    Char[] readBuffer = new Char[256];
                    int count = streamRead.Read(readBuffer, 0, 256);

                    while (count > 0)
                    {
                        String resultData = new String(readBuffer, 0, count);
                        rawResponse.Append(resultData);
                        count = streamRead.Read(readBuffer, 0, 256);
                    }
                }
            }
            return rawResponse.ToString();
        }



        public static void SaveXml(string Res, string securityToken, string filename)
        {
            //string newFileName = Res + DateTime.Now.ToString("hh:mm:ss");

            //string activeDir = ConfigurationManager.AppSettings["FZSaveUrl"] + DateTime.Now.ToString("dd-MMMM-yyyy") + @"\" + securityToken + @"\";
            //DirectoryInfo objDirectoryInfo = new DirectoryInfo(activeDir);
            //if (!Directory.Exists(objDirectoryInfo.FullName))
            //{
            //    Directory.CreateDirectory(activeDir);
            //}
            //try
            //{
            //    XDocument xmlDoc = XDocument.Parse(Res);

            //    xmlDoc.Save(activeDir + @"\" + filename + ".xml");
            //}
            //catch (Exception ex)
            //{
            //}
        }


          


    }
}
