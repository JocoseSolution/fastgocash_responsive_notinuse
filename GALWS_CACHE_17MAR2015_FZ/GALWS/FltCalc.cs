﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections;

namespace STD.BAL
{
    public class FltCalc
    {
        //public Hashtable Fligt_Markup_Calc(DataSet ds, Double T_BaseFare, Double T_Tax, Double T_Fare, string Airline, string UID, string D_ID, string UserType, string OWNER_ID)
        //{
        //    Double TotAfterMarkup;
        //    string id = "";
        //    string SearchVal = "";
        //    string Filter = "";
        //    string Table = "";
        //    Hashtable HashTblMrk = new Hashtable();
        //    string Admin_MarkupType= "N",Dist_MarkupType = "N",Agnt_MarkupType="N";
        //    double Admin_MarkupVal=0.0,Dist_MarkupVal = 0.0,Agnt_MarkupVal=0.0;
        //    string Admin_MarkUpON=null, Dist_MarkUpON=null, Agnt_MarkUpON=null;
        //    LoginBAL objLoginBAL = new LoginBAL();
        //    //SP_DIST_FLT_GETMARKUP - Get Rows from Markup Table where APPLIEDON in (@DistrID,'ALL')
        //    try
        //    {
        //        #region DEFINE USERTYPE

        //        SearchVal = objLoginBAL.getLoginType(OWNER_ID);
        //        if ((UserType == "EC") && (D_ID != OWNER_ID) && (D_ID != "SPRING")) //DI,TA,EC,SE
        //        {
        //            id = D_ID;
        //            SearchVal = "TA";
        //        }
        //        else if ((UserType == "EC") && (D_ID != OWNER_ID) && (D_ID == "SPRING")) //DI,TA,EC,SE
        //        {
        //            id = D_ID;
        //            SearchVal = "DI";
        //        }
        //        else if ((UserType == "SE") && (D_ID != OWNER_ID) && (D_ID != "SPRING"))
        //        {
        //            id = D_ID;
        //            SearchVal = "TA";
        //        }
        //        else if ((UserType == "SE") && (D_ID != OWNER_ID) && (D_ID == "SPRING")) //DI,TA,EC,SE
        //        {
        //            id = D_ID;
        //            SearchVal = "DI";
        //        }
        //        else if (UserType == "DI")
        //        {
        //            id = UID;
        //            SearchVal = "DI";
        //        }
        //        else if (UserType == "TA")
        //        {
        //            id = UID;
        //            SearchVal = "TA";
        //        }
        //        else if ((D_ID == "SPRING") && (D_ID == "SPRING"))
        //        {
        //            id = "";
        //            SearchVal = "AD";
        //        }
        //        #endregion
        //        // If Distributor is SPRING Then Fetch Details From 2 Tables
        //        #region Admin MARKUP
        //        if (SearchVal == "AD") //IN Case Spring's Staff is Making Ticket for particular Distributor
        //        {
        //            if (ds.Tables.Count >= 1)
        //            {
        //                if (ds.Tables["TBL_AIR_MRK_ADMIN"].Rows.Count > 0)
        //                {   //ASk ->APPLIED ON ALL  FROM ADMIN TABLE (Select("APPLIEDON='ALL' or DI)
        //                    Filter = "APPLIEDON='ALL' and (AIRLINECODE='ALL' or AIRLINECODE='" + Airline + "')";
        //                    Table = "TBL_AIR_MRK_ADMIN";
        //                    FIND_VAL(ds, Filter, Airline, Table, out Admin_MarkupType, out Admin_MarkupVal, out Admin_MarkUpON);
        //                }
        //                //CalCulate Markup
        //                //if ((Admin_MarkupType != "N") & (Admin_MarkUpON != null))
        //                //{
        //                    HashTblMrk = Cal_VAL(Admin_MarkupType, Admin_MarkupVal, Admin_MarkUpON, "N", 0, "NA", "N", 0, "NA", T_BaseFare, T_Tax, T_Fare);
        //                //}
        //            }
        //        }
        //        #endregion

        //        #region DISTRIBUTOR
        //        else if (SearchVal == "DI")
        //        {
        //            if (ds.Tables.Count > 1)
        //            {
        //                if (ds.Tables["TBL_AIR_MRK_ADMIN"].Rows.Count > 0)
        //                {
        //                    Filter = "APPLIEDON='ALL' and (AIRLINECODE='ALL' or AIRLINECODE='" + Airline + "')";
        //                    Table = "TBL_AIR_MRK_ADMIN";
        //                    FIND_VAL(ds, Filter, Airline, Table, out Admin_MarkupType, out Admin_MarkupVal, out Admin_MarkUpON);
        //                }

        //                if (ds.Tables["TBL_AIR_MRK_DIST"].Rows.Count > 0)
        //                {
        //                    Filter = "APPLIEDON='ALL' and (AIRLINECODE='ALL' or AIRLINECODE='" + Airline + "')";
        //                    Table = "TBL_AIR_MRK_DIST";
        //                    FIND_VAL(ds, Filter, Airline, Table, out Dist_MarkupType, out Dist_MarkupVal, out Dist_MarkUpON);
        //                }
        //            }
        //            //CalCulate Markup
        //            //if ((Admin_MarkupType != "N" & Admin_MarkUpON != null) || (Dist_MarkupType != "N" & Dist_MarkUpON != null))
        //            //{
        //                HashTblMrk = Cal_VAL(Admin_MarkupType, Admin_MarkupVal, Admin_MarkUpON, Dist_MarkupType, Dist_MarkupVal, Dist_MarkUpON, "N", 0, "NA", T_BaseFare, T_Tax, T_Fare);
        //            //}
        //        }
        //        #endregion

        //        #region AGENT MARKUP
        //        else if ((SearchVal == "TA"))
        //        {
        //            if (ds.Tables.Count > 1)
        //            {
        //                if (ds.Tables["TBL_AIR_MRK_ADMIN"].Rows.Count > 0)
        //                {
        //                    Filter = "APPLIEDON='ALL' and (AIRLINECODE='ALL' or AIRLINECODE='" + Airline + "')";
        //                    Table = "TBL_AIR_MRK_ADMIN";
        //                    FIND_VAL(ds, Filter, Airline, Table, out Admin_MarkupType, out Admin_MarkupVal, out Admin_MarkUpON);
        //                }
        //                if (ds.Tables["TBL_AIR_MRK_DIST"].Rows.Count > 0)
        //                {
        //                    Filter = "APPLIEDON='ALL' and (AIRLINECODE='ALL' or AIRLINECODE='" + Airline + "')";
        //                    Table = "TBL_AIR_MRK_DIST";
        //                    FIND_VAL(ds, Filter, Airline, Table, out Dist_MarkupType, out Dist_MarkupVal, out Dist_MarkUpON);
        //                }
        //                if (ds.Tables["TBL_AIR_MRK_AGENT"].Rows.Count > 0)
        //                { //Different
        //                    Filter = "USERID='" + id + "' and (AIRLINECODE='ALL' or AIRLINECODE='" + Airline + "')";
        //                    Table = "TBL_AIR_MRK_AGENT";
        //                    FIND_VAL(ds, Filter, Airline, Table, out Agnt_MarkupType, out Agnt_MarkupVal, out Agnt_MarkUpON);
        //                }
        //            }
        //            //Calculate Markup Val
        //            //if ((Admin_MarkupType != "N" & Admin_MarkUpON != null) || (Dist_MarkupType != "N" & Dist_MarkUpON != null) || (Agnt_MarkupType != "N" & Agnt_MarkUpON != null))
        //            //{
        //                HashTblMrk = Cal_VAL(Admin_MarkupType, Admin_MarkupVal, Admin_MarkUpON, Dist_MarkupType, Dist_MarkupVal, Dist_MarkUpON, Agnt_MarkupType, Agnt_MarkupVal, Agnt_MarkUpON, T_BaseFare, T_Tax, T_Fare);
        //            //}

        //        }
        //        #endregion
        //    }
        //    catch (Exception ex)
        //    {
        //        //throw ex;
        //    }
        //    return HashTblMrk;

        //}

        public static void FIND_VAL(DataSet ds, string Filter, string Airline, string Table, out string MarkupType, out Double MarkupVal, out string MarkUpON)
        {
            //For Agent Based on User ID
            DataRow[] Rows;
            string M_Type = "N";
            double M_Val=0.0;
            string M_ON ="NA";
            try
            {
                if (ds.Tables[Table].Select(Filter, "").Length > 0)
                {
                    Rows = ds.Tables[Table].Select(Filter, "");
                    for (int i = 0; i <= Rows.Length - 1; i++)
                    {
                        if ((((string)Rows[i]["AIRLINECODE"]) == Airline))
                        {
                            M_Type = Rows[i]["MARKUPTYPE"].ToString();
                            M_Val = Double.Parse(Rows[i]["MARKUPVALUE"].ToString());
                            M_ON = Rows[i]["MARKUPON"].ToString();
                            break;
                        }
                        else
                        {
                            if (Rows[i]["AIRLINECODE"].ToString() == "ALL")
                            {
                                M_Type = Rows[i]["MARKUPTYPE"].ToString();
                                M_Val = Double.Parse(Rows[i]["MARKUPVALUE"].ToString());
                                M_ON = Rows[i]["MARKUPON"].ToString();
                            }
                            else
                            {
                                M_Type = "N";
                                M_Val = 0.0;
                                M_ON = "NA";
                            }
                        }
                    }
                    MarkupType = M_Type;
                    MarkupVal = M_Val;
                    MarkUpON = M_ON;
                }
                else
                {
                    MarkupType = "N";
                    MarkupVal = 0.0;
                    MarkUpON = "NA";
                }
            }
            catch (Exception ex)
            {
                //throw ex;
                MarkupType = "N";
                MarkupVal = 0.0;
                MarkUpON = "NA";
            }
        }

        public static Hashtable Cal_VAL(string Admin_MarkupType, double Admin_MarkupVal, string Admin_MarkUpON, string Dist_MarkupType, double Dist_MarkupVal, string Dist_MarkUpON, string Agnt_MarkupType, double Agnt_MarkupVal, string Agnt_MarkUpON, double BaseFare, double TotalTax, double TotalFare)
        {
            double Tot_AD_Markup = 0.0,Tot_DI_Markup = 0.0,Tot_AG_Markup = 0.0;
            Hashtable hashtable = new Hashtable();
            try
            {
                if ((Admin_MarkupVal > 0) & (Admin_MarkupType != "N"))
                {
                    if ((Admin_MarkUpON == "TB"))
                    {
                        //If Single Markup Need to be Sent
                        Tot_AD_Markup = Cal_Markup(Admin_MarkupType, Admin_MarkupVal, BaseFare);
                    }
                    else if ((Admin_MarkUpON == "TF"))
                    {
                        Tot_AD_Markup = Cal_Markup(Admin_MarkupType, Admin_MarkupVal, TotalFare);
                    }
                    else if ((Admin_MarkUpON == "TT"))
                    {
                        Tot_AD_Markup = Cal_Markup(Admin_MarkupType, Admin_MarkupVal, TotalTax);
                    }
                    //  hashtable.Add("Admin_Markup", Tot_AD_Markup);
                }
                if ((Dist_MarkupVal > 0) & (Dist_MarkupType != "N"))
                {
                    if (Dist_MarkUpON == "TB")
                    {
                        Tot_DI_Markup = Cal_Markup(Dist_MarkupType, Dist_MarkupVal, BaseFare);
                    }
                    else if (Dist_MarkUpON == "TF")
                    {
                        Tot_DI_Markup = Cal_Markup(Dist_MarkupType, Dist_MarkupVal, TotalFare);
                    }
                    else if (Dist_MarkUpON == "TT")
                    {
                        Tot_DI_Markup = Cal_Markup(Dist_MarkupType, Dist_MarkupVal, TotalTax);
                    }
                }
                if ((Agnt_MarkupVal > 0) & (Agnt_MarkupType != "N"))
                {
                    if ((Agnt_MarkUpON == "TB"))
                    {
                        Tot_AG_Markup = Cal_Markup(Agnt_MarkupType, Agnt_MarkupVal, BaseFare);
                    }
                    else if ((Agnt_MarkUpON == "TF"))
                    {
                        Tot_AG_Markup = Cal_Markup(Agnt_MarkupType, Agnt_MarkupVal, TotalFare);
                    }
                    else if ((Agnt_MarkUpON == "TT"))
                    {
                        Tot_AG_Markup = Cal_Markup(Agnt_MarkupType, Agnt_MarkupVal, TotalTax);
                    }
                }
                hashtable.Add("Admin_Markup", Tot_AD_Markup);
                hashtable.Add("Dist_Markup", Tot_DI_Markup);
                hashtable.Add("Agnt_Markup", Tot_AG_Markup);
            }
            catch (Exception ex)
            {
               // throw ex;
            }
           //return TotMarkup; 
            return hashtable;
        }

        public static double Cal_Markup(string MarkupType, double MarkupVal, double Factor)
        {
            double TotMarkup =0.0;
            try
            {
                if (MarkupType.ToUpper() == "P")
                {//Percentage
                    TotMarkup = Math.Round(((Factor * MarkupVal) / 100 + TotMarkup),0);
                }
                else if (MarkupType.ToUpper() == "F")
                {//Fixed
                    TotMarkup =  Math.Round((MarkupVal + TotMarkup),0);
                }
            }
            catch (Exception ex)
            {
               // throw ex;
            }
            return TotMarkup;
        }
    }
}

